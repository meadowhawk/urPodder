# Use a Java base image
FROM eclipse-temurin:17-jre-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the Spring Boot application JAR file into the Docker image
ADD https://codeberg.org/meadowhawk/urPodder/releases/download/1.0.4/urPodder-1.0.4.jar urPodder.jar
ARG RAILWAY_VOLUME_MOUNT_PATH
ARG RAILWAY_SERVICE_NAME=urPodder-service
ARG URPODDER_PSWD=Ch4ng3UrPa$$w0rd
ARG PORT=8443

#Make dir for datbase TODO Remove for railway version..
#RUN mkdir -p data

# Set environment variables
ENV VOLUME_PATH=$RAILWAY_VOLUME_MOUNT_PATH
ENV SERVER_PORT=$PORT
ENV SPRING_PROFILES_ACTIVE=docker
ENV DB_PASSWORD=$URPODDER_PSWD
ENV DB_PATH=jdbc:h2:file:/data/urPodder.db
ENV LOGGING_LEVEL=INFO

EXPOSE $PORT

# Run the Spring Boot application when the container starts
CMD ["java", "-jar", "urPodder.jar"]

