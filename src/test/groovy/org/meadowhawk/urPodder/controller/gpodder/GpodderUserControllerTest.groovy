package org.meadowhawk.urPodder.controller.gpodder

import org.meadowhawk.urPodder.controller.AdminController
import org.meadowhawk.urPodder.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import spock.lang.Specification
import spock.mock.DetachedMockFactory

//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest(controllers = [GpodderUserController])
class GpodderUserControllerTest extends Specification {

    @Autowired
    UserService userService

    @Autowired
    private MockMvc mvc


//
//    def "Login request with correct credentials should return status OK"() {
//        given:
//        def username = "admin"
//        def password = "password"
//        def encoded = Base64.getEncoder().encodeToString((username + ":" + password).getBytes())
//
//        and:
////        userService.doSomethingWithUser() >> true
//    //Probably need to mock UrpUserDetailsService?
//        when:
//        def results = mvc.perform(
//                post('/api/2/auth/test/login.json')
//                        .header(HttpHeaders.AUTHORIZATION,"Basic "+encoded)
//                        .contentType(MediaType.APPLICATION_JSON))
////                        .content(toJson(request)))    // 4
//
//        then:
//        results.andExpect(status().isOk())                 // 5
//
////        and:
////        results.andExpect(jsonPath('$.registration_id').value('registration-id-1'))        // 5
////        results.andExpect(jsonPath('$.email_address').value('john.wayne@gmail.com'))
////        results.andExpect(jsonPath('$.name').value('John'))
////        results.andExpect(jsonPath('$.last_name').value('Wayne'))
//    }
//
//    def "when get is performed then the response has status 200 and content is 'Hello world!'"() {
//        expect: "Status is 200 and the response is 'Hello world!'"
//        mvc.perform(get("/api/2/auth/test/login.json"))
//                .andExpect(status().isOk())
////                .andReturn()
////                .response
////                .contentAsString == "Hello world!"
//    }
//
//    @TestConfiguration                                          // 6
//    static class StubConfig {
//        DetachedMockFactory detachedMockFactory = new DetachedMockFactory()
//
//        @Bean
//        UserService userService() {
//            return detachedMockFactory.Stub(UserService)
//        }
//    }
}