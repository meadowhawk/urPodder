package org.meadowhawk.urPodder.service

import groovy.json.JsonParserType
import groovy.json.JsonSlurper
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.DeviceRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.Device
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dao.domain.enums.DeviceType
import org.meadowhawk.urPodder.dao.domain.enums.EpisodeActionType
import org.meadowhawk.urPodder.dao.domain.enums.ReportType
import org.meadowhawk.urPodder.dto.EpisodeActionReq
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest
class ReportServiceTest extends Specification{
    @Autowired
    ReportService reportService
    @Autowired
    EpisodeService episodeService
    @Autowired
    UserDetailsRepo userDetailsRepo
    @Autowired
    DeviceRepo deviceRepo

    static def username = "tester3";
    static String deviceName ="gpodder-ubuntu3"
    static UserAuth auth

    @Shared
    boolean setup = false
    def setup() {
        if (!setup) { //setup user data to support case.
            UserProfile user = userDetailsRepo.findByUsername(username).orElseThrow { new Exception(username + " user missing from test database") };
            def newDevice = new Device(id: deviceName, user: user, type: DeviceType.DESKTOP, caption: "Test device")
            deviceRepo.save(newDevice)

            auth = getUserAuth(username)
        }
    }

    def "Test that create,get and update of LastListen"(){
        given: "Play activity exists for user."
        def username = "tester3"
        def ea = [new EpisodeActionReq(podcast: "http://feeds.thisisdistorted.com/xml/pure-trance-radio-podcast-with-solarstone?fromat=xml",
                episode: "https://audio.thisisdistorted.com/repository/audio/episodes/Solarstone_pres._Pure_Trance_Radio_Episode_398_192k-1710409198030795271-Mzg1NDAtMTU2ODY0NjMw.mp3",
                device: deviceName, action: EpisodeActionType.PLAY, timestamp: "2024-03-10T09:00:00", started: 600)]

        episodeService.updateEpisodes(auth, username, ea)

        when:
        reportService.updateUserReport(ReportType.LAST_LISTEN, username)
        sleep(2000)
        def report = reportService.getReportByUserAndType(ReportType.LAST_LISTEN, username)

        then:
        noExceptionThrown()

        assert !report.isBlank()
        JsonSlurper parser = new JsonSlurper().setType(JsonParserType.INDEX_OVERLAY);
        def json = parser.parseText(report)


        assert json.podcastUrl == "http://feeds.thisisdistorted.com/xml/pure-trance-radio-podcast-with-solarstone"
        assert json.episodeUrl == "https://audio.thisisdistorted.com/repository/audio/episodes/Solarstone_pres._Pure_Trance_Radio_Episode_398_192k-1710409198030795271-Mzg1NDAtMTU2ODY0NjMw.mp3"
        assert json.podcastName == "Pure Trance Radio Podcast with Solarstone"
        assert json.episodeTitle == "Pure Trance Radio Podcast 398"
        assert json.publishDate == "Thu, 14 Mar 2024 10:00:00 +0000"
        assert json.description.contains("A weekly podcast of progressive and uplifting Pure Trance music, presented by Solarstone.")
    }

    def "Test Get Last X listens for user"(){
        given:
        def username = "tester3"

        when:
        reportService.updateUserReport(ReportType.RECENT_EPISODES, username)

        def report = reportService.getReportByUserAndType(ReportType.RECENT_EPISODES, username)

        then:
        noExceptionThrown()
        assert !Objects.isNull(report)
        JsonSlurper parser = new JsonSlurper().setType(JsonParserType.INDEX_OVERLAY);
        def json = parser.parseText(report)
        assert json != null
    }

    def getUserAuth(String username){
        UserProfile user = userDetailsRepo.findByUsername(username).orElseThrow { new Exception(username + " user missing from test database") };
        UserProfile defaultUser2 = new UserProfile(id: user.id, username: user.username, password: "12345")
        new UserAuth(defaultUser2)
    }
}
