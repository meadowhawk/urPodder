package org.meadowhawk.urPodder.service

import org.meadowhawk.urPodder.dao.PodcastRepo
import org.meadowhawk.urPodder.dao.domain.Podcast
import org.meadowhawk.urPodder.util.URLSanitizer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class PodcastServiceTest extends Specification{

    @Autowired
    PodcastRepo podcastRepo

    @Autowired
    PodcastService podcastService

    def "Test createPodcastFromUrl"(){
        given:
        String url = "https://www.techdirt.com/feed"

        when:
        Podcast podcast = podcastService.createPodcastFromUrl(url)

        then:
        noExceptionThrown()
        assert podcastService != null
        assert podcast != null
        assert podcast.url.toString() == url
        assert podcast.website.toString() == "https://www.techdirt.com"
        assert podcast.description.isEmpty()
        assert podcast.logoUrl.toString() == "https://i0.wp.com/www.techdirt.com/wp-content/uploads/2022/02/cropped-techdirt-square-512x512-1.png?fit=32%2C32&ssl=1"
        assert podcast.title == "Techdirt"
    }

    def "Test createPodcast for existing and new."(){

        given:
        String existingPodcast = "https://www.techdirt.com/feed"
        URL newPodcast = URLSanitizer.sanitize("https://iotpodcast.com/feed/podcast/").get()

        //ensure that the new podcast is not already created before starting
        Optional<Podcast> newCheck = podcastRepo.findByUrl(newPodcast)
        newCheck.ifPresent {podcastRepo.delete(it)}


        when: "Calling to get an Existing Podcast"
        def existing = podcastService.getOrCreatePodcast(existingPodcast)

        then: "Existing Podcast returned"
        noExceptionThrown()
        assert existing != null
        assert existing.url.toString() == "https://www.techdirt.com/feed"

        when: "Calling to get an Existing Podcast"
        def newCreate = podcastService.getOrCreatePodcast(newPodcast)

        then:
        noExceptionThrown()
        assert newCreate != null
        assert newCreate.url == newPodcast
    }
}
