package org.meadowhawk.urPodder.service

import groovy.json.JsonParserType
import groovy.json.JsonSlurper
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.EpisodeAction
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dao.domain.enums.EpisodeActionType
import org.meadowhawk.urPodder.dao.domain.enums.FileUploadType
import org.meadowhawk.urPodder.dto.EpisodeActionReq
import org.meadowhawk.urPodder.dto.UploadSubChangeResponse
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

import static org.meadowhawk.urPodder.util.FeedParserTest.getTestTXT
import static org.meadowhawk.urPodder.util.FeedParserTest.testOpml

@SpringBootTest
@Stepwise
class EpisodeServiceTest extends Specification{
    @Autowired
    EpisodeService episodeService
    @Autowired
    UserDetailsRepo userDetailsRepo
    @Autowired
    SubscriptionService subscriptionService

    @Shared
    boolean setup = false

    def setup(){
        if(!setup) {
            def deviceID = "antennapod-droid"
            def deviceID2 = "gpodder-ubuntu"
            def authsername = "tester2"

            UserAuth auth = getUserAuth(authsername)

            subscriptionService.parseSubscriptions(auth, authsername, deviceID, testTXT, FileUploadType.TXT)
            //add subd for another device
            subscriptionService.parseSubscriptions(auth, authsername, deviceID2, testOpml, FileUploadType.OPML)

            def extras = """
http://feeds.thisisdistorted.com/xml/pure-trance-radio-podcast-with-solarstone
"""
            subscriptionService.parseSubscriptions(auth, authsername, deviceID, extras , FileUploadType.TXT)
        }
    }

    def "Test DTO to Entity Mapper"(){
        given:
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC)
        String username = "tester2"
        UserProfile user = userDetailsRepo.findByUsername(username).orElseThrow { new Exception(username + " user missing from test database") };
        String episodeUrl = "https://feeds.npr.org/2/s01e20.mp3"
        String device ="gpodder-ubuntu"
        EpisodeActionReq dto = new EpisodeActionReq(podcast: "https://feeds.npr.org/2/rss.xml", episode: episodeUrl, device: device,
                action: EpisodeActionType.DOWNLOAD, timestamp: "2009-12-12T09:00:00")

        when:
        EpisodeAction ea = episodeService.mapDto(user, dto)

        then:
        noExceptionThrown()
        ea.modifiedAt != null
        ea.modifiedAt.isAfter(now)
        ea.action == EpisodeActionType.DOWNLOAD
        ea.clientTimestamp.isBefore(now)
        ea.podcastId >0
        ea.episodeUrl == episodeUrl
        ea.deviceId == device

    }

    def "Test add episodes"(){
        given:
        long start = Instant.now().toEpochMilli()
        def username = "tester2"
        def username2 = "tester1"
        UserAuth auth = getUserAuth(username)
        UserAuth auth2 = getUserAuth(username2)
        String device ="gpodder-ubuntu"
        String device2 = "antennapod-droid"
        String device3 = "antennapod-iphone"

        List<EpisodeActionReq> requests = [
                new EpisodeActionReq(podcast: "http://feeds.thisisdistorted.com/xml/pure-trance-radio-podcast-with-solarstone?fromat=xml",
                        episode: "https://audio.thisisdistorted.com/repository/audio/episodes/Solarstone_pres._Pure_Trance_Radio_Episode_398_192k-1710409198030795271-Mzg1NDAtMTU2ODY0NjMw.mp3",
                        device: device, action: EpisodeActionType.PLAY, timestamp: "2024-03-10T09:00:00", started: 600),
                new EpisodeActionReq(podcast: "http://static.aboveandbeyond.nu/grouptherapy/podcast.xml", episode: "https://traffic.libsyn.com/secure/anjunabeats/ABGT481_minipod.m4a",
                        device: device, action: EpisodeActionType.PLAY, timestamp: "2022-12-12T10:00:00", started: 1200),
                new EpisodeActionReq(podcast:"https://feeds.megaphone.fm/darknetdiaries",
                        episode: "https://www.podtrac.com/pts/redirect.mp3/pdst.fm/e/chrt.fm/track/G481GD/traffic.megaphone.fm/ADV1546511926.mp3?updated=1710362066",
                        device: device2, action: EpisodeActionType.DOWNLOAD, timestamp: "2024-03-10T09:00:00"),
                new EpisodeActionReq(podcast: "http://feeds.feedburner.com/linuxoutlaws", episode: "http://content.sixgun.org/linuxoutlaws370a.mp3",
                        device: device, action: EpisodeActionType.DELETE, timestamp: "2024-03-14T10:00:00") //Podcast not in DB but should be added when found missing.
        ]

        List<EpisodeActionReq> requests2 = [
            new EpisodeActionReq(podcast:"https://feeds.megaphone.fm/darknetdiaries",
                    episode: "https://www.podtrac.com/pts/redirect.mp3/pdst.fm/e/chrt.fm/track/G481GD/traffic.megaphone.fm/ADV7390770931.mp3?updated=1704060905",
                    device: device3, action: EpisodeActionType.DOWNLOAD, timestamp: "2024-03-10T09:00:00")
        ]

        when:
        UploadSubChangeResponse resp = episodeService.updateEpisodes(auth, username, requests)

        then:
        noExceptionThrown()
        assert resp.timestamp > start
        assert resp.updateUrls.size() == 1
        def updatedList = []
        resp.updateUrls.each {updatedList += it[1]}
        assert updatedList.contains("http://feeds.thisisdistorted.com/xml/pure-trance-radio-podcast-with-solarstone")

        when: "Adding for another user"
        UploadSubChangeResponse resp2 = episodeService.updateEpisodes(auth2, username2, requests2)

        then: "Still works as expected."
        noExceptionThrown()
        assert resp2.timestamp > start
        assert resp2.updateUrls.size() == 0
    }

    def "Test get with various query Options."(){ //this test relies on data created from the prior test.
        given:
        def deviceID = "antennapod-droid"
        def deviceID2 = "gpodder-ubuntu"
        def authsername = "tester2"

        UserProfile user = userDetailsRepo.findByUsername(authsername).orElseThrow{ new Exception(authsername + " user missing from test database")};
        UserProfile defaultUser2 = new UserProfile(id:user.id, username:user.username, password: "12345")
        UserAuth auth = new UserAuth (defaultUser2)

        expect:
        try{
             def episodes = episodeService.getEpisodes(auth, username, deviceId,since,podcast,aggregated)
            assert episodes.size() == expectedResultCt
        } catch (UrPodderAppRestException ex) {
            if(expectException == false) println(ex.getMessage())
            assert expectException
        }

        where:
        testCase                    || username    || deviceId              || since         || aggregated   || expectException  || expectedResultCt || podcast
        "invalid user creds"        || "tester"    || null                  || 0             || false        || true             || 0                || null
        "Valid user creds"          || "tester2"   || null                  || 0             || false        || false            || 4                || null
        "Valid user w podcast"      || "tester2"   || null                  || 0             || false        || false            || 1                || "http://feeds.thisisdistorted.com/xml/pure-trance-radio-podcast-with-solarstone"
        "Valid user w/device"       || "tester2"   || "antennapod-droid"    || 0             || false        || false            || 1                || null
        "Not users device"          || "tester2"   || "tester-mymac"        || 0             || false        || false            || 0                || null
        "Valid user & since"        || "tester2"   || "gpodder-ubuntu"      || 1710057600    || false        || false            || 2                || null
    }

    def getUserAuth(String username){
        UserProfile user = userDetailsRepo.findByUsername(username).orElseThrow { new Exception(username + " user missing from test database") };
        UserProfile defaultUser2 = new UserProfile(id: user.id, username: user.username, password: "12345")
        new UserAuth(defaultUser2)
    }
}
