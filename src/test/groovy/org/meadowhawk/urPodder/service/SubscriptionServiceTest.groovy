package org.meadowhawk.urPodder.service

import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.DeviceRepo
import org.meadowhawk.urPodder.dao.SubscriptionRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.Device
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dao.domain.enums.DeviceType
import org.meadowhawk.urPodder.dao.domain.enums.FileUploadType
import org.meadowhawk.urPodder.dto.UploadSubChangeRequest
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import spock.lang.Specification

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

import static  org.meadowhawk.urPodder.util.FeedParserTest.*

@SpringBootTest
class SubscriptionServiceTest extends Specification{
    UserProfile defaultUser = new UserProfile(id:1, username:"admin", password: "12345")
    UserAuth defaultUserAuth = new UserAuth (defaultUser)

    @Autowired
    SubscriptionService subscriptionService
    @Autowired
    UserDetailsRepo userDetailsRepo
    @Autowired
    DeviceRepo deviceRepo
    @Autowired
    SubscriptionRepo subscriptionRepo

    def setupSpec(){
        //Create test data if not present.
    }
    /*
    Testing all aspects of the process of adding/removing subscriptions
     */
    def "Test MakeSubscriptionChanges: Neg tests"(){
        given:

            def device = new Device(id: deviceName)
            long start = Instant.now().toEpochMilli()
            UserProfile user = userDetailsRepo.findByUsername(authUser).orElseThrow{ new Exception(authUser + " user missing from test database")};
            UserProfile defaultUser2 = new UserProfile(id:user.id, username:user.username, password: "12345")
            UserAuth defaultUserAuth2 = new UserAuth (defaultUser2)
        expect:
            try {
                def resp = subscriptionService.makeSubscriptionChanges(defaultUserAuth2, username,deviceName,changeRequest)
                assert !expectException
                assert resp != null
                assert resp.timestamp >start

            }
            catch (UrPodderAppRestException ex) {
                if(expectException == false) println(ex.getMessage())
                assert expectException
                assert ex.responseStatus == expectedStatus
                assert ex.message == expectedMessage
            }


        where:
        testCase                                || authUser   || username   || deviceName    || expectException || expectedStatus           || expectedMessage                                  ||   changeRequest
        "Mismatch User/url username"            || "tester1"  || "tester2"  || "none"        || true            || HttpStatus.BAD_REQUEST   || "Bad Input" ||  new UploadSubChangeRequest()
        "url in both add & remove is ignored"   || "tester1"  || "tester1"  || "admin-droid" || true            || HttpStatus.BAD_REQUEST   || "Bad Input, add and remove have matches urls"    ||  new UploadSubChangeRequest(add: ["https://example.com/rss"], remove: ["https://example.com/rss"])
    }

    def "Test makeSubscriptionChanges: happy path" (){
        given:
        long start = Instant.now().toEpochMilli()
        UserProfile user = userDetailsRepo.findByUsername("tester1").orElseThrow{ new Exception("tester1 user missing from test database")}
        UserProfile defaultUser2 = new UserProfile(id:user.id, username:user.username, password: "12345")
        UserAuth defaultUserAuth2 = new UserAuth (defaultUser2)
        def deviceId = "admin-droid"

        when:
        def response = subscriptionService.makeSubscriptionChanges(defaultUserAuth2, "tester1",deviceId,new UploadSubChangeRequest(add: ['https://feeds.npr.org/2/rss.xml','https://www.techdirt.com/feed'], remove: []))

        then:
        noExceptionThrown()
        assert response != null
        assert response.timestamp >start
        assert deviceRepo.findById(deviceId).isPresent()
        assert response.updateUrls.size() == 2
    }

    def "Test returnUserSubscriptions"(){
        //Need to test with since =0 and well as a couple different time to ensure that removes and adds are correct
        given:
        def userTestAccount = "tester3"
        def fourDaysAgo = LocalDateTime.now().minusDays(4)
        def sixDaysAgo = LocalDateTime.now().minusDays(6)
        UserProfile user = userDetailsRepo.findByUsername(userTestAccount).orElseThrow{ new Exception("tester3 user missing from test database")}
        UserProfile userProfile = new UserProfile(id:user.id, username:user.username, password: "12345")
        UserAuth auth = new UserAuth (userProfile)
        def deviceId = "tester2-iphone"
        def deviceId2 = "device2-desktop"
        //Create a 2ndDevice
        def device2 = deviceRepo.save(new Device(id: deviceId2, caption: "Device #2", type: DeviceType.DESKTOP, user: user))

        //Add some Subscriptions with various mod times.
        def response = subscriptionService.makeSubscriptionChanges(auth, userTestAccount,deviceId,
                new UploadSubChangeRequest(add: ['https://feeds.npr.org/2/rss.xml','https://www.techdirt.com/feed',
                                                 'https://feeds.twit.tv/latest.xml','https://feeds.twit.tv/howin.xml','https://feeds.twit.tv/uls.xml'], remove: []))
        assert response.updateUrls.size() == 5
        //Make a couple over 4 days and one removed.

        def win = (subscriptionRepo.findByUserIdAndRefUrlAndDeviceId(user.id, URI.create("https://feeds.twit.tv/howin.xml").toURL(), deviceId)).get()
        win.removed = true
        subscriptionRepo.save(win)

        def uls = (subscriptionRepo.findByUserIdAndRefUrlAndDeviceId(user.id, URI.create("https://feeds.twit.tv/uls.xml").toURL(), deviceId)).get()
        uls.createdAt =sixDaysAgo
        uls.modifiedAt = sixDaysAgo
        subscriptionRepo.save(uls)

        def npr = (subscriptionRepo.findByUserIdAndRefUrlAndDeviceId(user.id, URI.create("https://feeds.npr.org/2/rss.xml").toURL(), deviceId)).get()
        npr.createdAt =sixDaysAgo
        npr.modifiedAt = sixDaysAgo
        subscriptionRepo.save(npr)


        when: "Since == 0 return all active subscriptions. "
        def respAll = subscriptionService.returnUserSubscriptionChanges(auth, userTestAccount, deviceId2, 0)

        then: "Gets 4 subs"
        noExceptionThrown()
        assert respAll.add.size() == 4
        assert respAll.remove.size() == 0


        when: "Since is 4 days ago, return all changes for past 4 days only." //1710949129
        def resp4days = subscriptionService.returnUserSubscriptionChanges(auth, userTestAccount, deviceId2, fourDaysAgo.toInstant(ZoneOffset.UTC).toEpochMilli()/1000 as long)

        then: "Gets 2 add and one remove."
        noExceptionThrown()
        assert resp4days.add.size() == 2
        assert resp4days.remove.size() == 1
        assert resp4days.remove.contains("https://feeds.twit.tv/howin.xml")
    }

    def "Test parseSubscriptions With valid and invalid input strings"(){
        given:
        UserProfile user = userDetailsRepo.findByUsername("tester2").orElseThrow{ new Exception("tester2 user missing from test database")}
        UserProfile userProfile = new UserProfile(id:user.id, username:user.username, password: "12345")
        UserAuth auth = new UserAuth (userProfile)
        Optional<Device> initialDevice = deviceRepo.findByIdAndUserId(deviceID, user.id)
        //assert  initialDevice.isEmpty() //Shouldn't be there yet

        expect:
        try {
            subscriptionService.parseSubscriptions(auth, "tester2", deviceID, input, type)
            //Search DB to ensure sub and podcast was created if made it this far.
            Optional<Device> updatedDevice = deviceRepo.findByIdAndUserId(deviceID, user.id)
            assert updatedDevice.isPresent()

            def devSubs = subscriptionRepo.findByUserIdAndRemovedAndDeviceId(user.id,false,updatedDevice.get().id)
            //verify that sub count created is correct
            assert devSubs.size() == resultCount

        }catch(UrPodderAppRestException ex){
            if(expectException == false) println(ex.getMessage())
            assert expectException
            assert ex.getResponseStatus() == HttpStatus.BAD_REQUEST
        }

        where:
        deviceID            || type                || input        || resultCount  || expectException  || expectedMessage
        "tester2-iphone12"  || FileUploadType.OPML || testOpml     || 5            || false            || ""
        "tester2-iphone2"   || FileUploadType.OPML || testJson     || 0            || true             || "Content is not allowed in prolog."
        "tester2-droid1"    || FileUploadType.JSON || testJson     || 2            || false            || ""
        "tester2-droid2"    || FileUploadType.JSON || testOpml     || 0            || true             || "Invalid content format"
        "tester2-laptop1"   || FileUploadType.TXT  || testTXT      || 4            || false            || ""
        "tester2-laptop2"   || FileUploadType.TXT  || testJson     || 0            || true             || "Invalid content format"
    }
}
