package org.meadowhawk.urPodder.util

import com.apptasticsoftware.rssreader.Item
import com.apptasticsoftware.rssreader.RssReader
import spock.lang.Specification

class PodcastUtilTest extends Specification{


    def "Test podcast library"(){

        given:
//        String URL = "https://www.techdirt.com/feed/"
        String URL = "https://feeds.transistor.fm/aws-morning-brief"
        RssReader rssReader = new RssReader()

        when:
        List<Item> items = rssReader.read(URL).toList()

        then:
        noExceptionThrown()
        assert items != null
    }
}
