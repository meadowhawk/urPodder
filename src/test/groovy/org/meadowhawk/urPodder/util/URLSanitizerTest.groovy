package org.meadowhawk.urPodder.util

import spock.lang.Specification

class URLSanitizerTest extends Specification{


    def "Test Sanitize with invalid url missing http etc"(){
        given:

        Optional<URL> url =  URLSanitizer.sanitize(testUrl)

        expect:
        assert url.isPresent() == isPresent
        if(isPresent)
            assert url.get().toString() == expectedUrl

        where:
        testUrl                                                 || isPresent    || expectedUrl
        "https://blog.meadowhawk.xyz/"                          || true         || "https://blog.meadowhawk.xyz/"
        "https://blog.meadowhawk.xyz/feeds/rss.xml"             || true         || "https://blog.meadowhawk.xyz/feeds/rss.xml"
        "https://blog.meadowhawk.xyz/feeds/test?val=123"        || true         || "https://blog.meadowhawk.xyz/feeds/test"
        "blog.meadowhawk.xyz"                                   || false        || ""
        "test"                                                  || false        || ""
    }

    def "Test parseUrl"(){
        given:

        Optional<URL> url =  URLSanitizer.parseUrl(testUrl)

        expect:
        assert url.isPresent() == isPresent
        if(isPresent)
            assert url.get().toString() == expectedUrl

        where:
        testUrl                                                 || isPresent    || expectedUrl
        "https://blog.meadowhawk.xyz/"                          || true         || "https://blog.meadowhawk.xyz/"
        "https://blog.meadowhawk.xyz/feeds/rss.xml"             || true         || "https://blog.meadowhawk.xyz/feeds/rss.xml"
        "blog.meadowhawk.xyz"                                   || false        || ""
        "test"                                                  || false        || ""
    }
}
