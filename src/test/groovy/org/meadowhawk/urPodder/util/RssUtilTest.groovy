package org.meadowhawk.urPodder.util

import spock.lang.Specification

/**
 * Integration Tests
 */
class RssUtilTest extends Specification{

    def "Test get Rss Podcast Item"(){
        given:
        def podcastUrl = "https://feeds.twit.tv/twit.xml"
        def episodeUrl = "https://pdst.fm/e/pscrb.fm/rss/p/cdn.twit.tv/libsyn/twit_965/b26992aa-b1b1-4f90-a2b7-efc6a7d4b42a/R1_twit0965.mp3"

        when:
        def item = RssUtil.createPodcastItemFromUrl(podcastUrl, episodeUrl)

        then:
        noExceptionThrown()
        assert item.episodeTitle == "TWiT 965: Baby's First Layoff - Apple Vision Pro, Google Retires Cache links"
        assert item.podcastName == "This Week in Tech (Audio)"
        assert item.description.contains("<li>The panel gives their thoughts on the Apple Vision Pro </li>")
        assert item.publishDate == "Sun, 04 Feb 2024 20:02:07 PST"
        assert item.podcastUrl == podcastUrl
        assert item.episodeUrl == episodeUrl
    }
}
