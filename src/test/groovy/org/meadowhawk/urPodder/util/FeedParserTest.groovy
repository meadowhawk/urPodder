package org.meadowhawk.urPodder.util

import org.meadowhawk.urPodder.dao.domain.Subscription
import org.meadowhawk.urPodder.dao.domain.enums.FileUploadType
import spock.lang.Shared
import spock.lang.Specification

class FeedParserTest extends Specification{



    def "test Json Parsing"(){
        when:
        List<Subscription> subs = FeedParser.parseJsonFeed(testJson)

        then:
        noExceptionThrown()
        subs.size() == 2
    }

    def "test OPML Parsing"(){
        when:
        List<Subscription> subs = FeedParser.parseOpmlFeed(testOpml)

        then:
        noExceptionThrown()
        subs.size() == 5
    }

    def "test txt Parsing"(){
        when:
        List<Subscription> subs = FeedParser.parseTextFeed(testTXT)

        then:
        noExceptionThrown()
        subs.size() == 4
    }

    def "NegativeTests"(){
        given:
        def subs = []
        expect:
        try{
            switch (type) {
                case FileUploadType.JSON:
                    subs = FeedParser.parseJsonFeed(input)
                    break
                case FileUploadType.OPML:
                    subs = FeedParser.parseOpmlFeed(input)
                    break
                case FileUploadType.TXT:
                    subs = FeedParser.parseTextFeed(input)
                    break
            }
            assert subs.size() == resultCount
        }catch(Exception ex){
            if(expectException == false) println(ex.getMessage())
            assert expectException
            assert ex.message == expectedMessage
        }

        where:
        type                || input        || resultCount  || expectException  || expectedMessage
        FileUploadType.OPML || testOpml     || 5            || false            || ""
        FileUploadType.OPML || testJson     || 0            || true             || "Invalid content format"
        FileUploadType.JSON || testJson     || 2            || false            || ""
        FileUploadType.JSON || testOpml     || 0            || true             || "Invalid content format"
        FileUploadType.TXT  || testTXT      || 4            || false            || ""
        FileUploadType.TXT  || testJson     || 0            || true             || "Invalid content format"

    }

    static final testJson = """
      [{
      "website":"http://sixgun.org",
      "description":"The hardest-hitting Linux podcast around",
      "title":"Linux Outlaws",
      "author":"Sixgun Productions",
      "url":"http://feeds.feedburner.com/linuxoutlaws",
      "position_last_week":1,
      "subscribers":1954,
      "mygpo_link":"http://gpodder.net/podcast/11092",
      "logo_url":"http://sixgun.org/files/linuxoutlaws.jpg",
      "scaled_logo_url":"http://gpodder.net/logo/64/fa9fd87a4f9e488096e52839450afe0b120684b4.jpg"
   },
   {
      "website":"http://static.aboveandbeyond.nu/grouptherapy/podcast.xml",
      "description":"Group Therapy is the weekly radio show from Above & Beyond also known as ABGT",
      "title":"Above &amp; Beyond: Group Therapy",
      "author":"Above & Beyond",
      "url":"http://static.aboveandbeyond.nu/grouptherapy/podcast.xml",
      "position_last_week":2,
      "mygpo_link":"http://gpodder.net/podcast/11092",
      "logo_url":"http://static.aboveandbeyond.nu/assets/logos/Group_Therapy_Podcast_600x600.jpg"
   }
   ]
"""

    static final  testOpml  ="""
    <?xml version='1.0' encoding='UTF-8' standalone='no' ?>
<opml version="2.0">
  <head>
    <title>AntennaPod Subscriptions</title>
    <dateCreated>05 Dec 23 16:49:57 -0500</dateCreated>
  </head>
  <body>
    <outline text="Above &amp; Beyond: Group Therapy" title="Above &amp; Beyond: Group Therapy" type="rss" xmlUrl="http://static.aboveandbeyond.nu/grouptherapy/podcast.xml" htmlUrl="http://static.aboveandbeyond.nu/grouptherapy/podcast.xml" />
    <outline text="AWS Morning Brief" title="AWS Morning Brief" type="rss" xmlUrl="https://feeds.transistor.fm/aws-morning-brief" htmlUrl="https://www.lastweekinaws.com" />
    <outline text="Celtic Christmas Music" title="Celtic Christmas Music" type="rss" xmlUrl="https://celticchristmaspodcast.com/rss" htmlUrl="http://celticchristmaspodcast.com" />
    <outline text="CoRecursive: Coding Stories" title="CoRecursive: Coding Stories" type="rss" xmlUrl="https://corecursive.libsyn.com/feed" htmlUrl="http://corecursive.com" />
    <outline text="Darknet Diaries" title="Darknet Diaries" type="rss" xmlUrl="https://feeds.megaphone.fm/darknetdiaries" htmlUrl="https://darknetdiaries.com/" />
      </body>
</opml>
"""

    static final testTXT = """
https://feeds.npr.org/2/rss.xml
https://www.techdirt.com/feed
https://feeds.twit.tv/latest.xml
https://feeds.twit.tv/howin.xml
"""
}
