package org.meadowhawk.urPodder.config

import groovy.util.logging.Slf4j
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.filter.CommonsRequestLoggingFilter

/**
 * Request logging filter, for logging request details.
 */
@Slf4j
@Configuration
class RequestLoggingFilterConfig {

    @Bean
    CommonsRequestLoggingFilter logFilter() {
        CommonsRequestLoggingFilter filter
                = new CommonsRequestLoggingFilter()
        filter.setIncludeQueryString(true)
        if(LoggerFactory.getLogger("org.springframework.web.filter.CommonsRequestLoggingFilter").isTraceEnabled()){
            filter.setIncludePayload(true)
            filter.setMaxPayloadLength(10000)
            filter.setBeforeMessagePrefix("\n================BEFORE================\n")
            filter.setBeforeMessageSuffix("\n================AFTER================\n")
            filter.setIncludeHeaders(true)
        }



        return filter
    }
}
