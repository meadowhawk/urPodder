package org.meadowhawk.urPodder.config

import groovy.util.logging.Slf4j
import org.codehaus.groovy.runtime.StackTraceUtils
import org.meadowhawk.urPodder.dto.ErrorMessage
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.springframework.dao.NonTransientDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.AccessDeniedException
import org.springframework.validation.BindException
import org.springframework.validation.ObjectError
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.resource.NoResourceFoundException

/**
 * Spring Exception handlers.
 */
@ControllerAdvice
@Slf4j
class GlobalExceptionHandler {

    @ExceptionHandler(value = [UrPodderAppRestException.class])
    ResponseEntity<ErrorMessage> apiRestException(UrPodderAppRestException ex, WebRequest request) {

        log.error("Rest Exception Error: {}     request : {}", ex, request)
        return new ResponseEntity<>(new ErrorMessage(message: ex.getMessage(), validationErrors: ex.validationErrors), ex.getResponseStatus());
    }

    @ExceptionHandler(value = [NoResourceFoundException.class])
    ResponseEntity<Object> handleBadUrlsException(Exception ex, WebRequest request) {
        return new ResponseEntity<>(new ErrorMessage( message: "Not Found"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = [Exception.class] )
    ResponseEntity<Object> handleException(Exception ex, WebRequest request) {
        log.warn("Unexpected Exception: {}",ex, request)
        if(log.isInfoEnabled())
            StackTraceUtils.sanitize(ex).printStackTrace()
        return new ResponseEntity<>(new ErrorMessage( message: "Unexpected Error."), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = [AccessDeniedException.class] )
    ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
        return new ResponseEntity<>(new ErrorMessage( message: "Access Denied."), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = [BindException.class])
    ResponseEntity<ErrorMessage> apiRestException(BindException ex, WebRequest request) {
        log.info("Rest Exception Error: {}     request : {}", ex, request);

        Map<String, String> validationErrors = new HashMap<>();
        for (ObjectError err:  ex.getAllErrors()){
            validationErrors.put(err.getObjectName(), err.getDefaultMessage()) ;
        }

        return new ResponseEntity<>(new ErrorMessage(message: ex.getMessage(), validationErrors: ex.validationErrors), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = [NonTransientDataAccessException.class, IllegalArgumentException.class])
    ResponseEntity<ErrorMessage> sqlExceptions(Exception ex, WebRequest request) {
        log.error("Rest Exception Error: {}     request : {}", ex, request);
        if(log.isInfoEnabled())
            StackTraceUtils.sanitize(ex).printStackTrace()

        return new ResponseEntity<>(new ErrorMessage(message: "Invalid Key values or missing data input."), HttpStatus.BAD_REQUEST);
    }
}
