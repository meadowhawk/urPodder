package org.meadowhawk.urPodder.config

import groovy.util.logging.Slf4j
import jakarta.servlet.Filter
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletException
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletRequest
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component


/**
 * Used while debugging http requests and is skipped when debug is not on..
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
@Slf4j
 class RequestDebugFilter implements Filter {

    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(log.isDebugEnabled()) {
            HttpServletRequest sreq = request as HttpServletRequest
            StringBuilder sb = new StringBuilder("==== Logging Filter ====\n")

            sb.append("COOKIES:\n\t[")
            sreq.cookies.each { cookie ->
                cookie.attributes.each {
                    sb.append(it.key).append("|").append(it.value).append(",")
                }
            }
            sb.append("]\n")
            sb.append("SESSION:\n\t[")
            sreq.getSession().attributeNames.each { name ->
                sb.append(name).append("|").append(sreq.getSession().getAttribute(name)).append(",")
            }
            sb.append("]\n")
            sb.append("HEADERS:\n\t[")
            sreq.getHeaderNames().each { name ->
                sb.append(name).append("|").append(sreq.getHeader(name)).append(",")
            }
            sb.append("]\n")
            log.debug(sb.toString())
        }
        chain.doFilter(request, response)
    }
}
