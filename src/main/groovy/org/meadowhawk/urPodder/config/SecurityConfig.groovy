package org.meadowhawk.urPodder.config

import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.ProviderManager
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

/**
 * Configures Spring Security
 */
@SuppressWarnings('unused')
@Configuration
@EnableAsync
@EnableWebSecurity
class SecurityConfig {

    @Autowired
    UserDetailsRepo userDetailsRepo;

    @Bean
    static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10)
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http.csrf(AbstractHttpConfigurer::disable)
            .authorizeHttpRequests((authorize) -> {
                authorize.requestMatchers("/").permitAll()
                authorize.requestMatchers("/error").permitAll()
                authorize.requestMatchers("/social/**").permitAll()
                authorize.requestMatchers("/actuator/**").permitAll()
                authorize.requestMatchers("/h2-console/*").permitAll()
                authorize.requestMatchers("/admin/make-admin").permitAll()
                authorize.requestMatchers("/swagger-ui/**").permitAll()
                authorize.requestMatchers("/v3/api-docs/**").permitAll()
                authorize.anyRequest().authenticated()
                }).httpBasic(Customizer.withDefaults())
                .rememberMe {
                    it.alwaysRemember(true)
                            .tokenValiditySeconds(60*5)
                        .key("Deadmau5")
                    }
        return http.build()
    }

    /**
     * Enables h2-console
     * @return
     */
    @Bean
    WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().requestMatchers(new AntPathRequestMatcher("/h2-console/**"))
    }

    @Bean
    AuthenticationManager authenticationManager(
            UserDetailsService userDetailsService,
            PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider()
        authenticationProvider.setUserDetailsService(userDetailsService)
        authenticationProvider.setPasswordEncoder(passwordEncoder)

        return new ProviderManager(authenticationProvider)
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.sessionManagement(httpSecuritySessionManagementConfigurer ->
                httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.ALWAYS))
        return http.build()
    }

    @Bean
    ApplicationListener<AuthenticationSuccessEvent> successEvent() {
        return (event) -> {
            System.out.println("Success Login " + event.getAuthentication().getClass().getSimpleName() + " - " + event.getAuthentication().getName())
        }
    }

    @Bean
    ApplicationListener<AuthenticationFailureBadCredentialsEvent> failureEvent() {
        return (event) -> {
            System.err.println("Bad Credentials Login " + event.getAuthentication().getClass().getSimpleName() + " - " + event.getAuthentication().getName())
        }
    }
}
