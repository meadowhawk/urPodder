package org.meadowhawk.urPodder.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Configuration for Swagger docs to provide better info.
 */
@Configuration
class OpenAPIConfig {
    @Bean
    public OpenAPI urPodderOpenAPI() {
        Contact contact = new Contact()
        contact.setName("Meadowhawk")
        contact.setUrl("https://codeberg.org/meadowhawk/urPodder")

        License gplLicense = new License().name("GNU GPL v3").url("https://choosealicense.com/licenses/gpl-3.0/")

        Info info = new Info()
                .title("urPodder API Server")
                .version("1.0")
                .contact(contact)
                .description("This API provides a limited implementation of the gPodder API which allows both AntennaPod and gPodder desktop app to sync as the older gpodder.net use to do.")
                .summary("Refer to the projects git repo for details, https://codeberg.org/meadowhawk/urPodder")
                .license(gplLicense)

        return new OpenAPI().info(info)

    }
}
