package org.meadowhawk.urPodder.config.security

import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.AuthorityRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

/**
 * Service that provides user lookup from the database.
 */
@Component
class UrpUserDetailsService implements UserDetailsService{
    UserDetailsRepo userDetailsRepo
    AuthorityRepo authorityRepo

    UrpUserDetailsService(UserDetailsRepo userDetailsRepo, AuthorityRepo authorityRepo){
        this.userDetailsRepo = userDetailsRepo
        this.authorityRepo =  authorityRepo
    }

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (Objects.isNull(username))
            return null
        Optional<UserProfile>  user = userDetailsRepo.findByUsername(username)
        if(user.isEmpty())
            return null
        user.get().authorities += authorityRepo.findById(user.get().id).get()
        return new UserAuth(user.get())
    }
}
