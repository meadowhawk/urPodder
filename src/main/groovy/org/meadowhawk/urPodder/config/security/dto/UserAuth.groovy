package org.meadowhawk.urPodder.config.security.dto

import org.meadowhawk.urPodder.dao.domain.Authority
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User;

/**
 * Authenticated user passed into to Controllers when auth was successful.
 */
class UserAuth extends User {
    Long userId
    UserAuth(UserProfile up) {
        super(up.username, up.password, toGrantedAuthorities( up.getAuthorities()))
        this.userId = up.id
    }

    /**
     * Sets authorities transfered from UserProfile.
     * @param authorities
     * @return
     */
    static  Set<GrantedAuthority> toGrantedAuthorities(List<Authority> authorities){
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        authorities.forEach(authority ->
                grantedAuthorities.add(new SimpleGrantedAuthority(authority.authority)));
        return grantedAuthorities;
    }
}
