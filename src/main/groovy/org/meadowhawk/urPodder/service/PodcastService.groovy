package org.meadowhawk.urPodder.service

import com.apptasticsoftware.rssreader.Channel
import com.apptasticsoftware.rssreader.Item
import com.apptasticsoftware.rssreader.RssReader
import org.meadowhawk.urPodder.dao.PodcastRepo
import org.meadowhawk.urPodder.dao.domain.Podcast
import org.meadowhawk.urPodder.exception.UrPodderException
import org.meadowhawk.urPodder.util.URLSanitizer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PodcastService {

    @Autowired
    PodcastRepo podcastRepo

    Podcast getOrCreatePodcast(String rssUrl) throws UrPodderException{
        Optional<URL> verifiedUrl =URLSanitizer.sanitize(rssUrl)
        if(verifiedUrl.isEmpty()) throw new UrPodderException("Invalid url")

        Optional<Podcast> existingPodcast = podcastRepo.findByUrl(verifiedUrl.get())
        existingPodcast.orElseGet{podcastRepo.save(createPodcastFromUrl(rssUrl))}
    }

    Podcast getOrCreatePodcast(URL rssUrl) throws UrPodderException{
        Objects.requireNonNull(rssUrl, "Invalid rss url supplied.")

        Optional<Podcast> existingPodcast = podcastRepo.findByUrl(rssUrl)
        existingPodcast.orElseGet {podcastRepo.save(createPodcastFromUrl(rssUrl))}
    }

    Podcast createPodcastFromUrl(URL urlValue) throws UrPodderException{
        RssReader rssReader = new RssReader()
        List<Item> items = rssReader.read(urlValue.toString()).toList()

        //Get Channel info from first item
        if(items.isEmpty()) throw new UrPodderException("Rss feed seems to be missing items, can't parse.")
        Channel channel = items.get(0).getChannel()
        URL link = URLSanitizer.parseUrl(channel.getLink()).orElseGet {throw new UrPodderException("Invalid URL for podcast link, cant save podcast as it is required field.")}
        URL logoUrl = (channel.getImage().isPresent() && URLSanitizer.parseUrl(channel.getImage().get().getUrl()))?
                URLSanitizer.parseUrl(channel.getImage().get().getUrl()).get():null

        new Podcast(url: urlValue,  title: channel.getTitle(), description: channel.getDescription(), website: link, logoUrl: logoUrl , author: "")
    }

    //TODO: Figure out what the itunes reader is for, can rsReader parse it?
    Podcast createPodcastFromUrl(String urlValue) throws UrPodderException{
        Optional<URL> verifiedUrl =URLSanitizer.sanitize(urlValue)
        if(verifiedUrl.isEmpty()) throw new UrPodderException("Invalid url")

        createPodcastFromUrl(verifiedUrl.get())
    }
}
