package org.meadowhawk.urPodder.service

import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.DeviceRepo
import org.meadowhawk.urPodder.dao.SubscriptionRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.Device
import org.meadowhawk.urPodder.dao.domain.Podcast
import org.meadowhawk.urPodder.dao.domain.Subscription
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dao.domain.enums.DeviceType
import org.meadowhawk.urPodder.dao.domain.enums.FileUploadType
import org.meadowhawk.urPodder.dto.GetSubscriptionChangesResponse
import org.meadowhawk.urPodder.dto.UploadSubChangeRequest
import org.meadowhawk.urPodder.dto.UploadSubChangeResponse
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.meadowhawk.urPodder.exception.UrPodderException
import org.meadowhawk.urPodder.util.FeedParser
import org.meadowhawk.urPodder.util.URLSanitizer
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

@Service
class SubscriptionService {
    UserDetailsRepo userDetailsRepo
    SubscriptionRepo subscriptionRepo
    DeviceRepo deviceRepo
    PodcastService podcastService

    SubscriptionService(UserDetailsRepo userDetailsRepo, SubscriptionRepo subscriptionRepo, DeviceRepo deviceRepo, PodcastService podcastService){
        this.userDetailsRepo = userDetailsRepo
        this.subscriptionRepo = subscriptionRepo
        this.deviceRepo = deviceRepo
        this.podcastService = podcastService
    }

    /*
    - Only deltas are supported here. Timestamps are not supported, and are issued by the server.
    - URLs that are not allowed (currently all URLs that don’t start with either http or https) are rewritten to the empty string and are ignored by the Webservice.
     */
    @Transactional
    UploadSubChangeResponse makeSubscriptionChanges( UserAuth auth, String username, String deviceId, UploadSubChangeRequest subChangeReq) throws UrPodderAppRestException{
        if(auth.username != username || Objects.isNull(subChangeReq)) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.BAD_REQUEST)
        UserProfile user = userDetailsRepo.findById(auth.userId).orElseThrow { new UrPodderAppRestException("User not found", null, HttpStatus.NOT_FOUND)}
        //Check for add/remove dupes
        if(subChangeReq.add.intersect(subChangeReq.remove)) throw new UrPodderAppRestException("Bad Input, add and remove have matches urls", null, HttpStatus.BAD_REQUEST)

        Device device = getCreateDevice(user, deviceId)
        UploadSubChangeResponse resp = new UploadSubChangeResponse(timestamp: Instant.now().toEpochMilli())

        subChangeReq.add.each {add-> {
            Subscription match = user.getSubscriptions().find{it.refUrl.toExternalForm() == add && it.device.id == deviceId}
            if(Objects.isNull(match)) {
                Optional<URL> url = URLSanitizer.sanitize(add)
                if(url.isPresent()) {
                    Podcast podcast = podcastService.getOrCreatePodcast(url.get())
                    subscriptionRepo.save(new Subscription(device: device, refUrl: url.get(), user: user, podcastId: podcast.id, createdAt: LocalDateTime.now(ZoneOffset.UTC), modifiedAt: LocalDateTime.now(ZoneOffset.UTC)))
                    resp.updateUrls.add([add, url.get().toString()])
                }
            } else{ //IF an existing removed sub is added again, then mark it active again
                if(match.removed) {
                    match.removed = false
                    match.modifiedAt = LocalDateTime.now(ZoneOffset.UTC)
                    subscriptionRepo.save(match)
                }
            }
        }}

        subChangeReq.remove.each {remove-> {
            Optional<URL> rmUrl = URLSanitizer.sanitize(remove)
            if(rmUrl.isPresent()) {
                Optional<Subscription> subResp = subscriptionRepo.findByUserIdAndRefUrlAndDeviceId(user.id, rmUrl.get(), device.id)
                //Mark sub as removed. Can't delete it since wouldn't be able to report changes to other devices.
                subResp.ifPresent {sub ->
                    sub.setRemoved(true)
                    sub.modifiedAt = LocalDateTime.now(ZoneOffset.UTC)
                    subscriptionRepo.save(sub)
                }
            }
        }}
        return resp
    }

    /**
     * This returns subscription changes, adds and removes performed on devices other than the one in the parameters.
     * @param auth
     * @param username
     * @param deviceId - device that is syncing
     * @param since - time stamp, defaults to 0, meaning all changes
     * @return
     */
    GetSubscriptionChangesResponse returnUserSubscriptionChanges(UserAuth auth, String username, String deviceId, Long since){
        if(auth.username != username) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.BAD_REQUEST)
        List<String> adds = []
        List<String> removes = []
        if(since == 0 )
            subscriptionRepo.findByUserIdAndRemovedAndDeviceIdNot(auth.userId, false, deviceId).each { sub ->
                adds += sub.refUrl.toString()
            }
        else{
            LocalDateTime sinceLDT =
                    LocalDateTime.ofInstant(Instant.ofEpochSecond(since), ZoneId.of("UTC"))
            List<Subscription> changes = subscriptionRepo.findByUserIdAndDeviceIdNotAndModifiedAtAfter(auth.userId, deviceId, sinceLDT)
            changes.each {
                if(it.removed) removes += it.refUrl.toString()
                else adds += it.refUrl.toString()
            }
        }

        new GetSubscriptionChangesResponse(timestamp: Instant.now().toEpochMilli(), add: adds, remove: removes)
    }

    /**
     * Parses upload files to add build subscriptions. Since a successful response for the controller returns nothing but a 200, theres nothing to return here.
     * @param auth
     * @param username
     * @param deviceId
     * @param body - the uploaded subscription data.
     * @param fileUploadType -enum
     * @throws UrPodderAppRestException
     */
    @Transactional
    void parseSubscriptions(UserAuth auth, String username, String deviceId, String body, FileUploadType fileUploadType) throws UrPodderAppRestException{
        if(auth.username != username ) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.UNAUTHORIZED)
        UserProfile user = userDetailsRepo.findById(auth.userId).orElseThrow { new UrPodderAppRestException("User not found", null, HttpStatus.UNAUTHORIZED)}
        List<String> subs = processInput(body, fileUploadType)
        if(subs.size()==0) return

        Device device = getCreateDevice(user, deviceId)

        //Iterate list of Subs and create if they arent already there.
        subs.each {newSub ->
            Subscription match = user.getSubscriptions().find{it.refUrl.toExternalForm() == newSub && it.device.id == deviceId}
            Optional<URL> url = URLSanitizer.sanitize(newSub)
            if(match == null){
                Podcast podcast = podcastService.getOrCreatePodcast(newSub)
                subscriptionRepo.save(new Subscription(device: device, refUrl: url.get(), user: user, podcastId: podcast.id, createdAt: LocalDateTime.now(ZoneOffset.UTC), modifiedAt: LocalDateTime.now(ZoneOffset.UTC)))
            }
        }

    }

    @Transactional
    Device getCreateDevice(UserProfile user, String deviceId){
        List<Device> devices = user.getDevices()
        def device  = devices.find {it.id  == deviceId}
        if(device == null){
            //Create Device
            def newDevice = new Device(id:deviceId, user: user, type: DeviceType.OTHER, caption: "")
            device = deviceRepo.save(newDevice)
        }
        device
    }

    /*
     * parseData to get sub URLs
     */
    static List<String> processInput(String body, FileUploadType fileUploadType) {
        List<String> subs = []
        try {
            switch (fileUploadType) {
                case FileUploadType.JSON:
                    subs = FeedParser.parseJsonFeed(body)
                    break;
                case FileUploadType.OPML:
                    subs = FeedParser.parseOpmlFeed(body)
                    break;

                case FileUploadType.TXT:
                    subs = FeedParser.parseTextFeed(body)
                    break;
            }
        } catch (UrPodderException ur){
            throw new UrPodderAppRestException("Invalid input. Perhaps you passed the wrong datatype? Valid types are Json, opml and text urls 1 per line",ur, HttpStatus.BAD_REQUEST)
        }
        subs
    }
}
