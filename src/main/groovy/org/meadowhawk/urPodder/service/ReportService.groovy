package org.meadowhawk.urPodder.service

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import jakarta.persistence.Tuple
import org.meadowhawk.urPodder.dao.EpisodeActionRepo
import org.meadowhawk.urPodder.dao.UserReportRepo
import org.meadowhawk.urPodder.dao.domain.EpisodeAction
import org.meadowhawk.urPodder.dao.domain.UserReport
import org.meadowhawk.urPodder.dao.domain.UserReportId
import org.meadowhawk.urPodder.dao.domain.enums.EpisodeActionType
import org.meadowhawk.urPodder.dao.domain.enums.ReportType
import org.meadowhawk.urPodder.dto.PodcastItem
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.meadowhawk.urPodder.util.RssUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

@Slf4j
@Service
class ReportService {
    @Autowired
    UserReportRepo userReportRepo
    @Autowired
    ObjectMapper om
    @Autowired
    EpisodeActionRepo episodeActionRepo

    /**
     * Builds stored reports for each user. Because the original API doesnt seem to save the episode data, the reports allow for
     * building that info and saving it because of the higher overhead of making many rss feed calls.
     * @param reportType
     * @param username
     * @return
     */
    String getReportByUserAndType(ReportType reportType, String username){
        Optional<UserReport>  report = userReportRepo.findByReportTypeAndUsername(reportType,username)
        report.isEmpty()? "": report.get().report //TODO: Find better response for empty? null, {} etc.
    }

    /**
     * Creates a static report that may get called frequently, but has high overhead to create. Anything that needs to request a podcast rss
     * feed is delayed so this allows caching those results so GET requests are as performant as expected.
     */
    void updateUserReport(ReportType type, String username){
        switch (type) {
            case ReportType.LAST_LISTEN:
                List<EpisodeAction> latest = episodeActionRepo.findByActionAndUsernameOrderByClientTimestampDesc(EpisodeActionType.PLAY, username)
                if(!latest.isEmpty()) {
                    PodcastItem podcastItem = (!latest.isEmpty()) ? RssUtil.createPodcastItemFromUrl(latest[0].podcastUrl, latest[0].episodeUrl) : null
                    String reportJson = om.writeValueAsString(podcastItem)
                    if(reportJson==null) {
                        log.info("No podcastItem was returned")
                        return
                    }

                    def usrReport = userReportRepo.findByReportTypeAndUsername(type,username)
                    UserReport report = usrReport.orElse(new UserReport(reportType: ReportType.LAST_LISTEN, username: username))

                    report.report = reportJson
                    userReportRepo.save(report)
                }
                break
            case ReportType.RECENT_PODCASTS:
                def results = buildLastNPodcastsReport(username)
                String reportJson = om.writeValueAsString(results)
                def usrReport = userReportRepo.findById(new UserReportId( type,username))
                UserReport report = usrReport.orElse(new UserReport(reportType: ReportType.RECENT_EPISODES, username: username))

                report.report = reportJson
                userReportRepo.save(report)
                break
            case ReportType.RECENT_EPISODES:
                //TODO: add search for last 5 unique podcast listens. select unique podcasts from episodes
                break
        }
    }

    List<PodcastItem>  buildLastNPodcastsReport(String username){
        List<PodcastItem> recents = []
        List<Tuple> resp = episodeActionRepo.findRecentEpisodesByUser(username, 5)
        resp.forEach {
            //TODO Consider checking list for dupe Podcasts and copying info when already requested.
            try {
                recents += RssUtil.createPodcastItemFromUrl(it.get('PODCASTURL'), it.get('EPISODEURL'))
            }catch( Exception e){
                def podurl = (it.get('PODCASTURL'))?it.get('PODCASTURL'):"Invalid podcast url"
                def epdurl = (it.get('EPISODEURL'))?it.get('EPISODEURL'):"Invalid episode url"
                log.warn("Exception parsing feed $podurl ep: $epdurl  Error: ${e.getMessage()}")

                throw new UrPodderAppRestException("Error processing RSS feed for $podurl > $epdurl",e, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        }

        recents
    }

    /**
     * Default manner of calling the report generation at runtime.
     * Wrapper for the report calls Async, its easier to test if its not async.
     * @param type
     * @param username
     */
    @Async
    void asyncReport(ReportType type, String username){
        this.updateUserReport(type, username)
    }
}
