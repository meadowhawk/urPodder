package org.meadowhawk.urPodder.service

import groovy.util.logging.Slf4j
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.DeviceRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.Device
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dao.domain.enums.DeviceType
import org.meadowhawk.urPodder.dto.AddDeviceRequest
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service

@Slf4j
@Service
class DeviceService {
    DeviceRepo deviceRepo

    UserDetailsRepo userDetailsRepo

    DeviceService(DeviceRepo deviceRepo, UserDetailsRepo userDetailsRepo){
        this.deviceRepo = deviceRepo
        this.userDetailsRepo = userDetailsRepo
    }


    Device addDevice(UserAuth auth, String username, String deviceid,AddDeviceRequest addDeviceRequest){
        if(auth.username != username ) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.BAD_REQUEST)
        UserProfile user = userDetailsRepo.findById(auth.userId).orElseThrow { new UrPodderAppRestException("User not found", null, HttpStatus.NOT_FOUND)}

        deviceRepo.save(
            new Device(user: user, type: DeviceType.forValues(addDeviceRequest.type), caption: addDeviceRequest.caption, id: deviceid)
        )
    }

    List<Device> getUserDevices(UserAuth auth, String username){
        if(auth.username != username) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.BAD_REQUEST)
        UserProfile user = userDetailsRepo.findById(auth.userId).orElseThrow { new UrPodderAppRestException("User not found", null, HttpStatus.NOT_FOUND)}

        def devices = user.getDevices()
        devices.stream().forEach {dev->
            dev.subCount = dev.subscriptions.size()
        }
        devices
    }
}
