package org.meadowhawk.urPodder.service

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import jakarta.validation.ConstraintViolation
import jakarta.validation.Validation
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.DeviceRepo
import org.meadowhawk.urPodder.dao.EpisodeActionRepo
import org.meadowhawk.urPodder.dao.PodcastRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.Device
import org.meadowhawk.urPodder.dao.domain.EpisodeAction
import org.meadowhawk.urPodder.dao.domain.Podcast
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dao.domain.enums.ReportType
import org.meadowhawk.urPodder.dto.EpisodeActionReq
import org.meadowhawk.urPodder.dto.UploadSubChangeResponse
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.meadowhawk.urPodder.exception.UrPodderException
import org.meadowhawk.urPodder.util.URLSanitizer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.data.domain.ExampleMatcher
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.stream.Collectors

@Service
class EpisodeService {
    private static final String dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    private static dateFormatter = DateTimeFormatter.ofPattern(dateTimeFormat).withZone(ZoneId.of("UTC"))
    @Autowired
    UserDetailsRepo userDetailsRepo

    @Autowired
    PodcastRepo podcastRepo

    @Autowired
    PodcastService podcastService

    @Autowired
    DeviceRepo deviceRepo

    @Autowired
    EpisodeActionRepo episodeActionRepo

    @Autowired
    ReportService reportService
    /**
     * Returns the last Played episode. I would like to apply some minimal listen time but there is no guarantee one will be provided.
     * Might want to revisit.
     * @param username
     * @return
     */
    String getLastListen(String username){
        reportService.getReportByUserAndType(ReportType.LAST_LISTEN, username)
    }

    /**
     * Returns recently listened to podcasts based off episode plays.
     * @param username
     * @return
     */
    String getRecentPodcasts(String username){
        reportService.getReportByUserAndType(ReportType.RECENT_EPISODES, username)
    }

    UploadSubChangeResponse updateEpisodes(UserAuth auth, String username, List<EpisodeActionReq> request){
        if(auth.username != username ) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.BAD_REQUEST)
        UserProfile user = userDetailsRepo.findById(auth.userId).orElseThrow { new UrPodderAppRestException("User not found", null, HttpStatus.NOT_FOUND)}

        UploadSubChangeResponse resp = new UploadSubChangeResponse(timestamp: Instant.now().toEpochMilli())
        request.each {
            //create/Save
           EpisodeAction episode = mapDto(user, it)
            //only include changes
            if(it.podcast != episode.podcastUrl)
                resp.updateUrls.add([it.podcast, episode.podcastUrl])

            episodeActionRepo.save(episode)
        }
        reportService.asyncReport(ReportType.LAST_LISTEN, username)
        reportService.asyncReport(ReportType.RECENT_PODCASTS, username)

        resp
    }

    /**
     *
     * @param auth
     * @param username
     * @param deviceId
     * @param since
     * @param podcast
     * @param aggregated - ignoring for now.
     * @return
     */
    List<EpisodeAction> getEpisodes(UserAuth auth, String username, String deviceId, long since, String podcast,  boolean aggregated){
        if(auth.username != username ) throw new UrPodderAppRestException("Bad Input", null, HttpStatus.BAD_REQUEST)
        UserProfile user = userDetailsRepo.findById(auth.userId).orElseThrow { new UrPodderAppRestException("User not found", null, HttpStatus.NOT_FOUND)}
        Podcast podcastData = (podcast !=null && !podcast.isBlank())?
                podcastRepo.findByUrl(URLSanitizer.sanitize(podcast).get()).orElseThrow {new UrPodderAppRestException("Invalid podcast referenced.",null, HttpStatus.BAD_REQUEST)}
                :null

        Long podcastId = (podcastData!=null)?podcastData.id:null
        EpisodeAction episodeQuery = new EpisodeAction(username: username, podcastId: podcastId)

        ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnorePaths("enabled","id","password", "createdAt","total","position",
                "started","position","total", "modifiedAt")

        if(deviceId!= null) {
            episodeQuery.deviceId = deviceId
        }

        Example<EpisodeAction> example = Example.of(episodeQuery,matcher)

        List<EpisodeAction> episodes = episodeActionRepo.findAll(example, Sort.by("clientTimestamp").descending())
        if(since>0) {
            def sinceTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(since), ZoneId.of("UTC"))
            return episodes.stream().filter {
                it.clientTimestamp.isAfter(sinceTime)
            }.collect(Collectors.toList())
        }
        if(aggregated){
            Map<String, EpisodeAction> aggregate = [:]
            episodes.forEach {
                //add url as key if not present add , if found compare timestamps, skip if not > then current.
            }
        }
        episodes
        //TODO Add aggregated support. If true then filter only latest for each episode.
    }

    /*
    select by user name, [username,device,podcast, modifiedAt] [username,device, ]
     */
    /**
     *
     * @param user
     * @param eaDto
     * @return
     * @throws UrPodderException
     */
    EpisodeAction mapDto(UserProfile user, EpisodeActionReq eaDto) throws UrPodderException{
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC)
        URL podcastUrl = URLSanitizer.sanitize(eaDto.podcast).orElseThrow {new UrPodderException("Invalid podcast URL referenced.")}
        URL episodeUrl = URLSanitizer.parseUrl(eaDto.episode).orElseThrow {new UrPodderException("Invalid episode URL referenced.")}

        Podcast podcastData =podcastService.getOrCreatePodcast(podcastUrl)
        Device deviceData = (eaDto.device != null && !eaDto.device.isBlank())?
                deviceRepo.findByIdAndUserId(eaDto.device, user.id).orElseThrow {new UrPodderException("Cant find referenced device.")}
                :null

        LocalDateTime convertedTimestamp = LocalDateTime.now(ZoneOffset.UTC)
        if(eaDto.timestamp != null) {
            convertedTimestamp =LocalDateTime.parse((!eaDto.timestamp.endsWith('Z')?eaDto.timestamp += 'Z':eaDto.timestamp), dateFormatter)
        }

        EpisodeAction episodeAction = new EpisodeAction(episodeUrl: episodeUrl.toString(),  podcastId: podcastData.id, action: eaDto.action, deviceId: deviceData?.id,
                username: user.username, started: eaDto.started, position:  eaDto.position, total: eaDto.total, clientTimestamp: convertedTimestamp,
                createdAt: now, modifiedAt: now, podcastUrl: podcastUrl.toString()
        )
        def validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<EpisodeAction>> violations = validator.validate(episodeAction);
        if(violations.size()>0)
            throw new UrPodderException("Invalid input: "+violations.toString())

        episodeAction
    }

    @Entity
    @Table(name = "Episode_Action")
    class EpisodeQuery{
        @Id
        String id
        String username
        Long podcastId
        String deviceId
        LocalDateTime modifiedAt
    }


}
