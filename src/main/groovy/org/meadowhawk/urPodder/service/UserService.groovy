package org.meadowhawk.urPodder.service

import org.meadowhawk.urPodder.dao.AuthorityRepo
import org.meadowhawk.urPodder.dao.UserDetailsRepo
import org.meadowhawk.urPodder.dao.domain.Authority
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.exception.UrPodderAppRestException
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService {
    final PasswordEncoder passwordEncoder
    final UserDetailsRepo  userDetailsRepo
    final AuthorityRepo authorityRepo

    UserService(PasswordEncoder passwordEncoder, UserDetailsRepo  userDetailsRepo,AuthorityRepo authorityRepo){
        this.passwordEncoder = passwordEncoder
        this.userDetailsRepo =userDetailsRepo
        this.authorityRepo = authorityRepo
    }
    /**
     * Allows a single admin user to be created when urPodder is first setup, after it will always return a 401
     * @param user
     */
    void createAdminUser(UserProfile user) {
         if(!getAuthorityRepo().findByAuthority("ADMIN").isEmpty()) throw new UrPodderAppRestException("Admin user already exists!", null, HttpStatus.FORBIDDEN)

        Objects.requireNonNull(user,"Invalid UserProfile Object.")
        user.setPassword(passwordEncoder.encode(user.getPassword()))
        UserProfile up = userDetailsRepo.save(user)
        Authority auth = new Authority(userId: up.id,  authority: "ADMIN")
        authorityRepo.save(auth)
    }

    void createUser(UserProfile user) {
        Objects.requireNonNull(user,"Invalid UserProfile Object.")
        user.setPassword(passwordEncoder.encode(user.getPassword()))
        UserProfile up = userDetailsRepo.save(user)
        Authority auth = new Authority(userId: up.id,  authority: "USER")
        authorityRepo.save(auth)
    }
}
