package org.meadowhawk.urPodder

import groovy.util.logging.Slf4j
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.SecurityScheme
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

import java.nio.file.Files

@SecurityScheme(name = "basic-auth-api", scheme = "basic", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER)
@Slf4j
@SpringBootApplication
class UrPodderApplication {

	static void main(String[] args) {
		log.info("Running path: {}",System.getProperty("user.dir"))
		SpringApplication.run(UrPodderApplication, args)
	}
}
