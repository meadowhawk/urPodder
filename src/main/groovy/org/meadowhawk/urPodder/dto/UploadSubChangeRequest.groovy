package org.meadowhawk.urPodder.dto

class UploadSubChangeRequest {
    List<String> add
    List<String> remove
}
