package org.meadowhawk.urPodder.dto

import org.meadowhawk.urPodder.dao.domain.EpisodeAction
import org.meadowhawk.urPodder.util.URLSanitizer

class LastEpisodeAction {
    String podcastName
    URL podcastUrl
    URL episodeUrl
    String episodeTitle
    String publishDate
    String description

    LastEpisodeAction(){

    }

    LastEpisodeAction(EpisodeAction episodeAction){

        this.podcastUrl = episodeAction.podcastUrl
        this.episodeUrl = URLSanitizer.parseUrl(episodeAction.episodeUrl).orElse(null)
        this.episodeTitle = ""
        this.podcastName = ""
        this.publishDate = ""
        this.description = ""
    }

}
