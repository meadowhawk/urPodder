package org.meadowhawk.urPodder.dto

class PodcastItem {
    String podcastName
    String podcastUrl
    String episodeUrl
    String episodeTitle
    String publishDate
    String description
}
