package org.meadowhawk.urPodder.dto

import org.meadowhawk.urPodder.dao.domain.enums.EpisodeActionType


/*
    podcast – The feed URL to the podcast feed the episode belongs to (required)
    episode – The media URL of the episode (required)
    device – The device ID on which the action has taken place (see Devices)
    action – One of: download, play, delete, new (required)
    timestamp – A UTC timestamp when the action took place, in ISO 8601 format
    started – Only valid for “play”. the position (in seconds) at which the client started playback. Requires position and total to be set.
    position – Only valid for “play”. the position (in seconds) at which the client stopped playback
    total – Only valid for “play”. the total length of the file in seconds. Requires position and started to be set.
 */
class EpisodeActionReq {
    String podcast
    String episode
    String device
    EpisodeActionType action
    String timestamp; //"2009-12-12T09:00:00" //TODO Add conversion to LocalDateTime  https://www.baeldung.com/spring-boot-formatting-json-dates
    long started
    long position
    long total
}
/*
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("+yyyy-MM-dd'T'HH:mm:ss'Z'")
        .withZone(ZoneId.of("UTC"));
    LocalDateTime date = LocalDateTime.parse("+2017-02-26T01:02:03Z", formatter);
 */