package org.meadowhawk.urPodder.dto

import org.meadowhawk.urPodder.dao.domain.EpisodeAction

class EpisodeActionsResp {
    List<EpisodeAction> actions
    long timestamp
}
