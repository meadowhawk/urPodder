package org.meadowhawk.urPodder.dto

class GetSubscriptionChangesResponse {
    List<String> add = []
    List<String> remove = []
    long timestamp;
}
