package org.meadowhawk.urPodder.dto

import com.fasterxml.jackson.annotation.JsonProperty

class UploadSubChangeResponse {
    long timestamp

    @JsonProperty("update_urls")
    List<List<String>> updateUrls = []
}
