package org.meadowhawk.urPodder.dto

class AddDeviceRequest {
    String caption
    String type
}
