package org.meadowhawk.urPodder.dto

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.Valid
import lombok.Builder
import lombok.Data
import org.springframework.validation.annotation.Validated

/**
 * ErrorMessage to be returned to api user.
 */
@Validated
class ErrorMessage {
    @JsonProperty("code")
    @Builder.Default
    Integer code = 1000

    @JsonProperty("message")
    String message

    @JsonProperty("validationErrors")
    @Valid
    Map<String, String> validationErrors

    ErrorMessage code(Integer code) {
      this.code = code
      return this
    }
}
