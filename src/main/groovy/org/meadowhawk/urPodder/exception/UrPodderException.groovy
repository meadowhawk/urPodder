package org.meadowhawk.urPodder.exception

class UrPodderException extends Exception{

    UrPodderException(String message){
        super(message)
    }

    UrPodderException(String message, Exception e){
        super(message,e)
    }
}
