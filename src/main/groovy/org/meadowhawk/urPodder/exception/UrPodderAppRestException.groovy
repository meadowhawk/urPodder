package org.meadowhawk.urPodder.exception

import jakarta.validation.constraints.NotNull
import lombok.Getter
import lombok.Setter
import org.springframework.http.HttpStatus

/**
 * Provides a general  exception that can be captured from the application and returned in  a REST response Handler.
 */

@Getter
@Setter
class UrPodderAppRestException extends Exception {

    /*Map of validation exceptions key=field, value = error msg*/
    Map<String, String> validationErrors = new HashMap<>()
    /* The status code to return to from the app API */
    @NotNull
    HttpStatus responseStatus;
    UrPodderAppRestException(String message, Throwable cause, HttpStatus responseStatus){
        super(message, cause)
        this.responseStatus = responseStatus;
    }

    UrPodderAppRestException(String message, Throwable cause, HttpStatus responseStatus, Map<String, String> validationErrors){
        super(message, cause)
        this.responseStatus = responseStatus
        this.validationErrors = validationErrors
    }

}
