package org.meadowhawk.urPodder.controller

import io.swagger.v3.oas.annotations.Operation
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
/**
 * Generates Informational root page
 */
@RestController
class RootController {
    @Value('${application.version}')
    String appVersion

    @Operation(hidden = true )
    @GetMapping("/")
    ResponseEntity<String> getRoot(){
        def welcomepage = """
<!DOCTYPE html>
<head>
    <style> 
        body {
        background-color: black;
            color: white;
        }   
    a {color:#4cb0ff    }
    </style>
</head>
<html>
<body>
    <h1>urPodder Home page</h1>
    <p>A self-hostable gPodder server.</p>
    <p>This unsecured page is provided for information and verification of install. Most of the api requires authentication.</p>
    <p>Some links for the site
        <ul>
            <li><a href="./actuator/mappings">Manager</a></li>
            <li><a href="./h2-console/">Database(if enabled/should be disabled in prod)</a></li>
            <li><a href="./swagger-ui/index.html">API Swagger Docs</a></li>
        </ul>
    </p>
    <p>Version: $appVersion</p>
</body>
</html> 
"""
        ResponseEntity.ok().body(welcomepage)
    }

    @Operation(hidden = true )
    @GetMapping("/error")
    ResponseEntity<String> getError(){
        ResponseEntity.ok().body()
    }
}
