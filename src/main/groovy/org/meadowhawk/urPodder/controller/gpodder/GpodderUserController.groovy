package org.meadowhawk.urPodder.controller.gpodder

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.meadowhawk.urPodder.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * gPodder Auth endpoints login/logout.
 */
@Tag(name = "Authentication", description = "Implementation of the gPodder authentication endpoints.")
@RestController
@RequestMapping("/api/2")
class GpodderUserController {
    SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler()

    @Autowired
    UserService userService

    /**
     * Log in the given user for the given device via HTTP Basic Auth. Simply reaching this point
     * indicates a successful login
     *
     * @param auth
     * @param username
     */
    @Operation(
            summary = "Login endpoint",
            description = "Basic Authentication which provides a session cookie for continued authentication access.")
    @SecurityRequirement(name = "basic-auth-api")
    @PostMapping(value = "/auth/{username}/login.json", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE,"plain/text"])
    ResponseEntity<String> gpodderLogin(Authentication auth, @PathVariable("username") String username){
        ResponseEntity.ok().body("")
    }

    /**
     * Ends session for loged-in user. Sessions only live for 5 minutes currently.
     */
    @Operation(
            summary = "Logout endpoint",
            description = "Ends the users session with the server via logout.")
    @PostMapping("/auth/{username}/logout.json")
    ResponseEntity<String> gpodderLogOut(Authentication auth, @PathVariable("username") String username,
                                         HttpServletRequest request, HttpServletResponse response){
        this.logoutHandler.setClearAuthentication(true);
        this.logoutHandler.logout(request, response, auth);
        ResponseEntity.ok().header("jsessionid", "").header("cookie","").header("set-cookie", "")
                .body("")
    }


}
