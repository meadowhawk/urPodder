package org.meadowhawk.urPodder.controller.gpodder

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.transform.CompileStatic
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.servlet.http.HttpServletRequest
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.domain.enums.FileUploadType
import org.meadowhawk.urPodder.dto.GetSubscriptionChangesResponse
import org.meadowhawk.urPodder.dto.UploadSubChangeRequest
import org.meadowhawk.urPodder.dto.UploadSubChangeResponse
import org.meadowhawk.urPodder.service.SubscriptionService
import org.meadowhawk.urPodder.util.ValidUserAuth
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.util.StreamUtils
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import java.nio.charset.StandardCharsets

/**
 * gPodder API implementation for Subscriptions.
 */
@Tag(name = "Subscription", description = "Implementation of the gPodder Subscription management endpoints.")
@SecurityRequirement(name = "basic-auth-api")
@CompileStatic
@RestController
class SubscriptionsController {
    @Autowired
    SubscriptionService subscriptionService

    @Autowired
    ObjectMapper om

    /**
     *
     * req:<code>
     *   {
     *       "add": ["http://example.com/feed.rss", "http://example.org/podcast.php"],
     *       "remove": ["http://example.net/foo.xml"]
     *   } </code>
     *
     * In positive responses the server returns a timestamp/ID that can be used for requesting changes since this upload in a subsequent API call.
     * In addition, the server sends a list of URLs that have been rewritten (sanitized, see bug:747) as a list of tuples with the key “update_urls”.
     * The client SHOULD parse this list and update the local subscription list accordingly (the server only sanitizes the URL, so the semantic “content”
     * should stay the same and therefore the client can simply update the URL value locally and use it for future updates.
     *
     * exResp: <code>
         {
             "timestamp": 1337,
             "update_urls":
             [
                [ "http://feeds2.feedburner.com/LinuxOutlaws?format=xml", "http://feeds.feedburner.com/LinuxOutlaws" ],
                 [ "http://example.org/podcast.rss ", "http://example.org/podcast.rss"      ]
             ]
         } </code>
     *
     * @param auth
     * @param username
     * @param deviceid
     * @param uploadSubChangeReq
     * @return
     */
    @Operation(
            summary = "Upload Subscription Changes",
            description = "Allows updating a devices subscriptions via a json body.<br><br><b>Note</b> this endpoint also has an exact twin that will accept the same body content with the request having a Content-Type=application/x-www-form-urlencoded. This is to provide support for linux gPodder desktop app which seems to mistakenly post json as url-form-encoded.")
    @PostMapping(value = "/api/2/subscriptions/{username}/{deviceid}.json", consumes = [MediaType.APPLICATION_JSON_VALUE])
    UploadSubChangeResponse uploadSubscriptionChanges(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @PathVariable("deviceid") String deviceid, @RequestBody UploadSubChangeRequest uploadSubChangeReq){
        subscriptionService.makeSubscriptionChanges(auth.getPrincipal() as UserAuth, username, deviceid, uploadSubChangeReq)
    }

    /**
     * gPodder sends json to this endpoint with the content-type == 'application/x-www-form-urlencoded' I don't understand it, but this makes it work.
     *
     * parse the request body sent in the wrong format.
     * @param auth
     * @param username
     * @param deviceid
     * @param request
     * @return
     */
    @Operation(
            summary = "Upload Subscription Changes",
            description = "Allows updating a devices subscriptions via a json body but with the content-type set as urlencoded. This is to provide support for linux gPodder desktop app which seems to mistakenly post json as url-form-encoded. Aside from content parsing this functions exactly as the twin endpoint.")
    @PostMapping(value = "/api/2/subscriptions/{username}/{deviceid}.json", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    UploadSubChangeResponse uploadSubscriptionChanges2(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @PathVariable("deviceid") String deviceid, HttpServletRequest request){
        String rawBody = StreamUtils.copyToString(request.getInputStream(),StandardCharsets.UTF_8)

        String decode = URLDecoder.decode(rawBody, StandardCharsets.UTF_8.toString())
        UploadSubChangeRequest convertedReq = om.readValue(decode, UploadSubChangeRequest.class)
        subscriptionService.makeSubscriptionChanges(auth.getPrincipal() as UserAuth, username, deviceid, convertedReq)
    }


    /**
     * Note: v1 api.
     * Upload Subscriptions of Device - the only v1 subscriptions endpoint that AntennaPod seems to still use.
     *
     * Upload the current subscription list of the given user to the server. The data should be provided either in OPML, JSON
     * or plaintext (one URL per line) format, and should be uploaded just like a normal PUT request (i.e. in the body of the request).
     *
     * For successful updates, the implementation always returns the status code 200 and the empty string (i.e. an empty HTTP body) as a result,
     * any other string should be interpreted by the client as an (undefined) error.
     * In case the device does not exist for the given user, it is automatically created.
     * @param auth
     * @param username
     * @param deviceId
     * @param body
     * @return
     */
    @Operation(
            summary = "Updates Subscription Contents",
            description = "Allows updating a devices subscriptions Wit a simple Json post.")
    @PutMapping(value = "/subscriptions/{username}/{deviceId}.json", consumes = [MediaType.APPLICATION_JSON_VALUE])
    ResponseEntity<String> updateSubscription(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @PathVariable("deviceId") String deviceId, @RequestBody String body){
        subscriptionService.parseSubscriptions(auth.getPrincipal() as UserAuth, username, deviceId, body, FileUploadType.JSON)
        ResponseEntity.ok().body("")
    }
    /**
     * In case the device does not exist for the given user, it is automatically created.
     * Note: v1 api.
     * @param auth
     * @param username
     * @param deviceId
     * @param body
     * @return
     */
    @Operation(
            summary = "Updates Subscription Contents",
            description = "Allows updating a devices subscriptions With a simple text post. Input is a line delimited list of URLs")
    @PutMapping(value = "/subscriptions/{username}/{deviceId}.txt", consumes = [MediaType.TEXT_PLAIN_VALUE])
    ResponseEntity<String> updateSubscriptionText(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @PathVariable("deviceId") String deviceId, @RequestBody String body){
        subscriptionService.parseSubscriptions(auth.getPrincipal() as UserAuth, username, deviceId, body, FileUploadType.TXT)
        ResponseEntity.ok().body("")
    }

    /**
     * Opml subscription update.
     * @param auth
     * @param username
     * @param deviceId
     * @param body
     * @return
     */
    @Operation(
            summary = "Updates Subscription Contents",
            description = "Allows updating a devices subscriptions by posting OPML formatted subscriptions list.")
    @PutMapping(value = "/subscriptions/{username}/{deviceId}.opml", consumes = [MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE])
    ResponseEntity<String> updateSubscriptionOpml(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @PathVariable("deviceId") String deviceId, @RequestBody String body){
        subscriptionService.parseSubscriptions(auth.getPrincipal() as UserAuth, username, deviceId, body, FileUploadType.OPML)
        ResponseEntity.ok().body("")
    }

    /**
     * Gets subscription changes.
     * @param auth
     * @param username
     * @param deviceId
     * @param since
     * @return
     */
    @Operation(
            summary = "Get Subscription For Device",
            description = "Retrieves list of subscriptions based on the 'since' timestamp, this is a long in epoch time, default is 0 which returns all subscription changes recorded.")
    @GetMapping("/api/2/subscriptions/{username}/{deviceId}.json")
    GetSubscriptionChangesResponse getSubscriptions(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @PathVariable("deviceId") String deviceId, @RequestParam("since") long since ){
        subscriptionService.returnUserSubscriptionChanges(auth.getPrincipal() as UserAuth, username, deviceId, since)
    }

}
