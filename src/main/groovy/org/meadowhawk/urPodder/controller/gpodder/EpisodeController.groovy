package org.meadowhawk.urPodder.controller.gpodder

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.servlet.http.HttpServletRequest
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.domain.EpisodeAction
import org.meadowhawk.urPodder.dto.EpisodeActionReq
import org.meadowhawk.urPodder.dto.EpisodeActionsResp
import org.meadowhawk.urPodder.dto.UploadSubChangeResponse
import org.meadowhawk.urPodder.service.EpisodeService
import org.meadowhawk.urPodder.util.ValidUserAuth
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.core.Authentication
import org.springframework.util.StreamUtils
import org.springframework.web.bind.annotation.*

import java.nio.charset.StandardCharsets
import java.time.Instant
/**
 * Implementation of gPoddder API for Episodes.
 */
@Tag(name = "Episodes", description = "Implementation of the gPodder Episode management endpoints.")
@SecurityRequirement(name = "basic-auth-api")
@Slf4j
@RestController
class EpisodeController {
    @Autowired
    EpisodeService episodeService
    @Autowired
    ObjectMapper om

    /**
     * updates Episode activity per gPodder API
     * Request Sample:
     * <code>
         [
             {
                 "podcast": "http://example.com/feed.rss",
                 "episode": "http://example.com/files/s01e20.mp3",
                 "device": "gpodder_abcdef123",
                 "action": "download",
                 "timestamp": "2009-12-12T09:00:00"
             },
             {
                 "podcast": "http://example.org/podcast.php",
                 "episode": "http://ftp.example.org/foo.ogg",
                 "action": "play",
                 "started": 15,
                 "position": 120,
                 "total":  500
             }
         ]
     * </code>
     *
     * @param auth
     * @param username
     * @param request - Json list of episodeREquests
     * @return see sample
     * <code>
     *  {
             "timestamp": 1337,
             "update_urls": [
                ["http://feeds2.feedburner.com/LinuxOutlaws?format=xml", "http://feeds.feedburner.com/LinuxOutlaws"],
                ["http://example.org/episode.mp3 ", "http://example.org/episode.mp3"]
             ]
        }
     * </code>
     */
    @Operation(
            summary = "Update Episode Actions",
            description = """Updates User Episode Actions and returns a last updated timestamp what should be saved by the client. Also returns any URLS that were changed when sanitized along with the cleaned url.
                    <br><br><b>Note:</b>this endpoint also has an exact twin that will accept the same body content with the request having a Content-Type=application/x-www-form-urlencoded. This is to provide support for linux gPodder desktop app which seems to mistakenly post json as url-form-encoded.""")
    @PostMapping(value = "/api/2/episodes/{username}.json", consumes = [MediaType.APPLICATION_JSON_VALUE])
    UploadSubChangeResponse updateEpisodes(@ValidUserAuth Authentication auth, @PathVariable("username") String username, @RequestBody List<EpisodeActionReq> request){
        episodeService.updateEpisodes(auth.getPrincipal() as UserAuth, username, request)
    }

    /**
     *
     * @param auth
     * @param username
     * @param request
     * @return
     */
    @Operation(
            summary = "Update Episode Actions",
            description = """Updates User Episode Actions and returns a last updated timestamp what should be saved by the client. Also returns any URLS that were changed when sanitized along with the cleaned url.
                    <br><br><b>Note:</b>this endpoint also has an exact twin that will accept the same body content with the request having a Content-Type=application/x-www-form-urlencoded. This is to provide support for linux gPodder desktop app which seems to mistakenly post json as url-form-encoded.""")
    @PostMapping(value = "/api/2/episodes/{username}.json", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    UploadSubChangeResponse updateEpisodesMisTyped(@ValidUserAuth Authentication auth, @PathVariable("username") String username, HttpServletRequest request){
        String rawBody = StreamUtils.copyToString(request.getInputStream(), StandardCharsets.UTF_8)
        log.debug("Got FormEncoded request: \n{}", rawBody)
        List<EpisodeActionReq> reqValues = om.readValue(rawBody, new TypeReference<List<EpisodeActionReq>>(){})

        episodeService.updateEpisodes(auth.getPrincipal() as UserAuth, username, reqValues)
    }

    /**
     * Episode search per gPodder API, many of the params are defaulted and/or not required.
     * @param auth
     * @param username
     * @param since = timestamp long, updates since time.
     * @param podcast =url of podcast (Optional)
     * @param deviceId - Optional
     * @param aggregated - Not supported yet.
     * @return
     */
    @Operation(
            summary = "Get Episode Actions",
            description = """Retrieves all episode updates from the 'since' timestamp. This can be more specific by supplying a device name and/or podcast url but neither are required.""")
    @GetMapping("/api/2/episodes/{username}.json")
    EpisodeActionsResp getEpisodes(@ValidUserAuth Authentication auth, @PathVariable("username") String username,
                                   @RequestParam(value = "since", required = false, defaultValue ="0") long since,
                                   @RequestParam(value = "podcast", required = false) String podcast,
                                   @RequestParam(value = "device",required = false) String deviceId,
                                   @RequestParam(value = "aggregated", required = false) boolean aggregated){

        List<EpisodeAction> actions = episodeService.getEpisodes(auth.getPrincipal() as UserAuth, username, deviceId, since, podcast, aggregated)
        new EpisodeActionsResp(timestamp: Instant.now().toEpochMilli(), actions: actions)
    }
}
