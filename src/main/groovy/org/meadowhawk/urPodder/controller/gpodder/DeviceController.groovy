package org.meadowhawk.urPodder.controller.gpodder


import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.servlet.http.HttpServletRequest
import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.dao.domain.Device
import org.meadowhawk.urPodder.dto.AddDeviceRequest
import org.meadowhawk.urPodder.service.DeviceService
import org.meadowhawk.urPodder.util.ValidUserAuth
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.util.StreamUtils
import org.springframework.web.bind.annotation.*

import java.nio.charset.StandardCharsets

/**
 * gPodder api for Device endpoints.
 */
@Tag(name = "Device", description = "Implementation of the gPodder Device management endpoints.")
@SecurityRequirement(name = "basic-auth-api")
@Slf4j
@RestController
@RequestMapping("/api/2")
class DeviceController {
    @Autowired
    ObjectMapper om

    DeviceService deviceService

    DeviceController(DeviceService deviceService){
        this.deviceService = deviceService
    }

    /**
     * Creates Devices per gPodder API
     * @param auth
     * @param username
     * @param deviceid
     * @param addDeviceRequest
     * @return
     */
    @Operation(
            summary = "Add user Device",
            description = "Cretes a new user Device if it doesn't exists, returns no data.<br><br><b>Note:</b>this endpoint also has an exact twin that will accept the same body content with the request having a Content-Type=application/x-www-form-urlencoded. This is to provide support for linux gPodder desktop app which seems to mistakenly post json as url-form-encoded.")
    @PostMapping(value = "/devices/{username}/{deviceid}.json", consumes = [MediaType.APPLICATION_JSON_VALUE])
    ResponseEntity<String> createDevice(@ValidUserAuth Authentication auth, @PathVariable("username") String username,
                                        @PathVariable("deviceid") String deviceid, @RequestBody AddDeviceRequest addDeviceRequest){

        deviceService.addDevice(auth.getPrincipal() as UserAuth, username,deviceid, addDeviceRequest)
        ResponseEntity.ok().body("")
    }

    /**
     * Alt impl of createDevice to support gPodder.
     * Note: gPodder makes Posts with Content-Type=application/x-www-form-urlencoded so to be backwards compatible this is also supported.
     * @param auth
     * @param username
     * @param deviceid
     * @param request - raw request so can grab the json contained in the post body.
     * @return
     */
    @Operation(
            summary = "Add user Device",
            description = """Cretes a new user Device if it doesn't exists, returns no data. This endpoint also has an exact twin that will accept the same body content with the request 
having a Content-Type=application/x-www-form-urlencoded. 
This is to provide support for linux gPodder desktop app which seems to mistakenly post json as url-form-encoded.""")
    @PostMapping(value = "/devices/{username}/{deviceid}.json", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    ResponseEntity<String> createDeviceMistTyped(@ValidUserAuth Authentication auth, @PathVariable("username") String username,
                                                 @PathVariable("deviceid") String deviceid, HttpServletRequest request){
        String rawBody = StreamUtils.copyToString(request.getInputStream(), StandardCharsets.UTF_8)
        String decode = URLDecoder.decode(rawBody, StandardCharsets.UTF_8.toString())
        log.info("Got FormEncoded request: \n{}", decode)

        AddDeviceRequest addDeviceRequest = om.readValue(decode, AddDeviceRequest.class)

        deviceService.addDevice(auth.getPrincipal() as UserAuth, username,deviceid, addDeviceRequest)
        ResponseEntity.ok().body("")
    }

    /**
     * Returns list of users devices.
     * @param auth
     * @param username
     * @return
     */
    @Operation(
            summary = "Get User Devices",
            description = "Returns a list of user devices.")
    @GetMapping("/devices/{username}.json")
    List<Device> listDevice(@ValidUserAuth Authentication auth, @PathVariable("username") String username) {
        deviceService.getUserDevices(auth.getPrincipal() as UserAuth, username)
    }
}
