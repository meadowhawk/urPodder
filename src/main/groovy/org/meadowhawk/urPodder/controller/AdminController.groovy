package org.meadowhawk.urPodder.controller

import groovy.util.logging.Slf4j
import io.swagger.v3.oas.annotations.Operation
import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.meadowhawk.urPodder.dto.CreateUserRequest
import org.meadowhawk.urPodder.service.UserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Slf4j
@RestController
@RequestMapping("/admin")
class AdminController {
    UserService userService

    AdminController(UserService userService){
        this.userService = userService
    }

    @Operation(
            summary = "Add AdminUser",
            description = "One time endpoint that will allow the admin to setup their account. If there os >0 admin users this will return a 401",
            tags = [ "User Admin"] )
    @PostMapping(value = "/make-admin", consumes = ["application/json"])
    UserProfile createAdmin(@RequestBody CreateUserRequest up){
        userService.createAdminUser(new UserProfile(username: up.username, password: up.password, enabled: "Y"))
    }

    @Operation(
            summary = "Add User",
            description = "Allows admin user to create new user",
            tags = [ "User Admin"] )
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/adduser", consumes = ["application/json"])
    UserProfile createUser(@RequestBody CreateUserRequest up){
        userService.createUser(new UserProfile(username: up.username, password: up.password, enabled: "Y"))
    }
}
