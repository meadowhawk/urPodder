package org.meadowhawk.urPodder.controller

import groovy.util.logging.Slf4j
import io.swagger.v3.oas.annotations.Operation
import org.meadowhawk.urPodder.service.EpisodeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Slf4j
@RequestMapping("/social")
@RestController
class SocialController {

    @Autowired
    EpisodeService episodeService

    @Operation(
            summary = "Users Last Played",
            description = "Displays the users last played podcast Episode.",
            tags = [ "Social"] )
    @GetMapping(value = "/{username}/last_play", produces = [MediaType.APPLICATION_JSON_VALUE])
    String lastPlayedEpisode(@PathVariable("username") String username){
        episodeService.getLastListen(username)
    }

    @Operation(
            summary = "Users Recent Podcast listens ",
            description = "Displays the users recent podcast listens with listening time over 5 mins.",
            tags = [ "Social"] )
    @GetMapping(value = "/{username}/recent_podcasts")
    String recentPodcastListens(@PathVariable("username") String username){
        episodeService.getRecentPodcasts(username)
    }
}
