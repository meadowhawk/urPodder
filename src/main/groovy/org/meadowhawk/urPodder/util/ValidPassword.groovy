package org.meadowhawk.urPodder.util

import jakarta.validation.Constraint
import jakarta.validation.Payload
import org.meadowhawk.urPodder.util.validation.PasswordConstraintValidator

import java.lang.annotation.*

@Documented
@Constraint(validatedBy = PasswordConstraintValidator.class)
@Target( [ElementType.FIELD, ElementType.ANNOTATION_TYPE] )
@Retention(RetentionPolicy.RUNTIME)
@interface ValidPassword {

    String message() default "Invalid Password"

    Class<?>[] groups()

    Class<? extends Payload>[] payload()
}
