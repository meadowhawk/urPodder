package org.meadowhawk.urPodder.util

import groovy.json.*
import groovy.xml.XmlParser
import org.meadowhawk.urPodder.exception.UrPodderException

/**
 * Util for parsing feed content  for some of the endpoints.
 */
class FeedParser {

    /**
     * Simply parses the input looking for the podcast urls to then process like other new adds.
     * @param input
     * @return
     */
    static List<String> parseTextFeed(String input){
        def results = []
        try {
            input.eachLine {
                if (!it.isBlank()) { //ignores blank lines.
                    def url = URLSanitizer.parseUrl(it)
                    results += (url.orElseThrow { new Exception("Invalid URL") }).toString()
                }
            }
        } catch(Exception e){
            throw new UrPodderException("Invalid content format", e)
        }
        results
    }

    /**
     * Simply parses the input looking for the podcast urls to then process like other new adds. field='xmlUrl' in this case
     * @param input
     * @return
     */
    static List<String> parseOpmlFeed(String input){
        def results = []
        def parser = new XmlParser()
        try {
            if (input.startsWith("\n")) input = input.substring(input.indexOf('\n') + 2)
            def doc = parser.parseText(input.stripLeading().trim());
            doc.body.outline.each { o ->
                results += o.attributes().get("xmlUrl")
            }
        } catch(Exception e){
            throw new UrPodderException("Invalid content format", e)
        }
        results
    }

    /**
     * Simply parses the input looking for the podcast urls to then process like other new adds. field='url' in this case
     * @param input
     * @return
     */
    static List<String> parseJsonFeed(String input){
        def results = []
        try {
            JsonSlurper parser = new JsonSlurper().setType(JsonParserType.INDEX_OVERLAY);
            def json = parser.parseText(input)
            println(json)
            json.each { item ->
                results += item.url
            }
        } catch(Exception e){
            throw new UrPodderException("Invalid content format", e)
        }
        results
    }
}
