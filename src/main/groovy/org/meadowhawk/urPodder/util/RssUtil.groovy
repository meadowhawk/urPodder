package org.meadowhawk.urPodder.util

import com.apptasticsoftware.rssreader.Channel
import com.apptasticsoftware.rssreader.Item
import com.apptasticsoftware.rssreader.RssReader
import org.meadowhawk.urPodder.dao.domain.Podcast
import org.meadowhawk.urPodder.dto.PodcastItem
import org.meadowhawk.urPodder.exception.UrPodderException

import java.util.stream.Collectors

class RssUtil {

    /**
     * Gets Podcast Data from rss feed.
     * @param urlValue
     * @return
     * @throws UrPodderException
     */
    static Podcast createPodcastFromUrl(URL urlValue) throws UrPodderException{
        RssReader rssReader = new RssReader()
        List<Item> items = rssReader.read(urlValue.toString()).toList()

        //Get Channel info from first item
        if(items.isEmpty()) throw new UrPodderException("Rss feed seems to be missing items, can't parse.")
        Channel channel = items.get(0).getChannel()
        URL link = URLSanitizer.parseUrl(channel.getLink()).orElseGet {throw new UrPodderException("Invalid URL for podcast link, cant save podcast as it is required field.")}
        URL logoUrl = (channel.getImage().isPresent() && URLSanitizer.parseUrl(channel.getImage().get().getUrl()))?
                URLSanitizer.parseUrl(channel.getImage().get().getUrl()).get():null

        new Podcast(url: urlValue,  title: channel.getTitle(), description: channel.getDescription(), website: link, logoUrl: logoUrl , author: "")
    }

    /**
     * Looks up RSS feed and find item data for the specified Item
     * @param urlValue
     * @param episodeUrl
     * @return
     */
    static PodcastItem createPodcastItemFromUrl(String urlValue, String episodeUrl){
        RssReader rssReader = new RssReader()
        List<Item> items = rssReader.read(urlValue.toString()).toList()
        if(items.isEmpty()) throw new UrPodderException("Rss feed seems to be missing items, can't parse.")

        PodcastItem item = new PodcastItem()
        def episode = items.stream().filter {
            (it.enclosure.isPresent() && it.enclosure.get().url == episodeUrl)
        }.collect(Collectors.toList())

        if(!episode.isEmpty()){
            item.episodeUrl = episodeUrl
            item.publishDate = episode[0].pubDate.orElse("")
            item.episodeTitle = episode[0].title.orElse("")
            item.description = episode[0].description.orElse("")
            item.podcastName = episode[0].channel.title
            item.podcastUrl = urlValue
        }
        item
    }

}
