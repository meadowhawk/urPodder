package org.meadowhawk.urPodder.util;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.meadowhawk.urPodder.util.validation.AuthenticationValidator;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AuthenticationValidator.class)
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD} )
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidUserAuth {
    String message() default "Invalid Authentication Session";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
