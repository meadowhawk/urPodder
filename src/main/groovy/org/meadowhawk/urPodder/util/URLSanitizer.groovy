package org.meadowhawk.urPodder.util

/**
 * Utils for cleaning up and validating URLs submitted to the system.
 */
class URLSanitizer {

    /**
     * Should return empty if the url doesn't start with http or https
     * @param urlString
     * @return Sanitized URL or empty
     */
    static Optional<URL> sanitize(String urlString){
        try {
            if(Objects.isNull(urlString) || urlString.isBlank()) return Optional.empty()
            if( !(urlString.toLowerCase().startsWith("http://") || urlString.toLowerCase().startsWith("https://"))) throw new MalformedURLException("Only support http[s]")
            //remove QueryParams for podcast links per gPodder practice..
            if(urlString.contains("?")) urlString = urlString.substring(0,urlString.indexOf('?'))
            URL parsedUrl = (new URI(Objects.requireNonNullElse(urlString,""))).toURL()
            return Optional.of((new URI(Objects.requireNonNullElse(urlString,""))).toURL())
        } catch (MalformedURLException e){
            return Optional.empty()
        }
    }

    /**
     * Safely Parses the URL to an Optional, this is java20+ friendly as old methods are being deprecated.
     * @param urlString - string representation of url
     * @return optional URL
     */
    static Optional<URL> parseUrl(String urlString){
        try {
            if (!(urlString.toLowerCase().startsWith("http://") || urlString.toLowerCase().startsWith("https://"))) throw new MalformedURLException("Only support http[s]")
            return Optional.of((new URI(Objects.requireNonNullElse(urlString, ""))).toURL())
        } catch (MalformedURLException e){
            return Optional.empty()
        }
    }

}
