package org.meadowhawk.urPodder.util.validation

import groovy.util.logging.Slf4j
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

import org.meadowhawk.urPodder.config.security.dto.UserAuth
import org.meadowhawk.urPodder.util.ValidUserAuth
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.Authentication

/**
 * Validates a users current Authentication to ensure they are logged in and valid, removes a bunch of boilerplate verifications.
 */
@Slf4j
class AuthenticationValidator implements ConstraintValidator<ValidUserAuth, Authentication> {
    @Override
    void initialize(ValidUserAuth constraintAnnotation) {
        super.initialize(constraintAnnotation)
    }

    @Override
    boolean isValid(Authentication userAuth, ConstraintValidatorContext context) {
        if (userAuth == null || userAuth instanceof AnonymousAuthenticationToken || userAuth.getPrincipal()==null || !(userAuth.getPrincipal() instanceof UserAuth)) {
            log.warn("Auth credentials missing or Anonymous")
            return false
        }
        return true
    }
}
