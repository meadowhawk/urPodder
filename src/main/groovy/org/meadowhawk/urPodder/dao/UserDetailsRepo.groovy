package org.meadowhawk.urPodder.dao

import org.meadowhawk.urPodder.dao.domain.UserProfile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.CrudRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository

@Repository
interface UserDetailsRepo extends CrudRepository<UserProfile, Long> {
    Optional<UserProfile> findByUsername(String username);
}
