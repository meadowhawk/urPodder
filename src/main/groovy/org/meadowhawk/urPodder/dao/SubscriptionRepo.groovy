package org.meadowhawk.urPodder.dao

import org.meadowhawk.urPodder.dao.domain.Subscription
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

import java.time.LocalDateTime

@Repository
interface SubscriptionRepo extends JpaRepository<Subscription, Long> {
    List<Subscription> findByUserIdAndRefUrl(Long userId, URL refUrl)
    Optional<Subscription> findByUserIdAndRefUrlAndDeviceId(Long userId, URL refUrl, String deviceId)

    List<Subscription> findByUserIdAndRemovedAndDeviceIdNot(Long userId, Boolean removed, String deviceId)
    List<Subscription> findByUserIdAndRemovedAndDeviceId(Long userId, Boolean removed, String deviceId)
    List<Subscription> findByUserIdAndDeviceIdNotAndModifiedAtAfter(Long userId, String deviceId, LocalDateTime timestamp)
}
