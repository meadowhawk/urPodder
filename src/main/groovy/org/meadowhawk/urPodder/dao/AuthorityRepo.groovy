package org.meadowhawk.urPodder.dao

import org.meadowhawk.urPodder.dao.domain.Authority
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AuthorityRepo extends JpaRepository<Authority, Long> {
    List<Authority> findByAuthority(String authority);
}
