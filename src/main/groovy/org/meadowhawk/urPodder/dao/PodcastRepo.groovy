package org.meadowhawk.urPodder.dao

import org.meadowhawk.urPodder.dao.domain.Podcast
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PodcastRepo extends JpaRepository<Podcast, Long> {
    Optional<Podcast> findByUrl(URL url)
}
