package org.meadowhawk.urPodder.dao

import org.meadowhawk.urPodder.dao.domain.UserReport
import org.meadowhawk.urPodder.dao.domain.UserReportId
import org.meadowhawk.urPodder.dao.domain.enums.ReportType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserReportRepo extends JpaRepository<UserReport, UserReportId> {
    Optional<UserReport> findByReportTypeAndUsername( ReportType reportType, String username)

}
