package org.meadowhawk.urPodder.dao.domain

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id


/*
Feed info from <channel/>
    <title/>
    <link/> maps to website
    <Description/>
    <author/> Optional
    <image><ulr/></image>  logo
 */
@Entity
class Podcast {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id
    URL url
    String title
    String description
    int subscribers = 1
    URL logoUrl
    URL website
    //mygpo_link not used here.
    String mygpoLink
    String author

}
