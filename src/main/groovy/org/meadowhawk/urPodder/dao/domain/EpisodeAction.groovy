package org.meadowhawk.urPodder.dao.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Transient
import jakarta.validation.constraints.NotNull
import lombok.Builder
import org.meadowhawk.urPodder.dao.domain.enums.EpisodeActionType

import java.time.LocalDateTime
import java.time.ZoneOffset

@Entity
class EpisodeAction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id

    @NotNull
    @JsonProperty(value = "episode")
    String episodeUrl

    EpisodeActionType action

    String deviceId

    @NotNull
    String username

    @JsonIgnore
    @NotNull
    Long podcastId

    @JsonProperty(value = "podcast")
    @NotNull
    String podcastUrl

    long started
    long position
    long total

    @JsonProperty("timestamp")
    @Builder.Default
    LocalDateTime clientTimestamp

    @JsonIgnore
    @JsonProperty("created")
    @Builder.Default
    LocalDateTime createdAt = LocalDateTime.now(ZoneOffset.UTC);

    @JsonIgnore
    @JsonProperty("modified")
    @Builder.Default
    LocalDateTime modifiedAt = LocalDateTime.now(ZoneOffset.UTC);

}
