package org.meadowhawk.urPodder.dao.domain.enums

import com.fasterxml.jackson.annotation.JsonCreator

enum FileUploadType {

    JSON,
    OPML,
    TXT

    @JsonCreator
    static FileUploadType forValues(String type) {
        for (FileUploadType cond : values()) {
            if (cond.name() == type.toUpperCase()) {
                return cond
            }
        }

        return null;
    }
}