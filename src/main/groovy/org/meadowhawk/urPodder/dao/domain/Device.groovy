package org.meadowhawk.urPodder.dao.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.CascadeType
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import jakarta.persistence.Transient
import jakarta.validation.constraints.Max
import lombok.Builder
import org.meadowhawk.urPodder.dao.domain.enums.DeviceType

@Entity
class Device {

    @Id
    String id       //: "abcdef",

    String caption  //": "gPodder on my Lappy",
    DeviceType type     // "laptop"


    String userAgent;  //Soon to be required, needs to be taken from the headers.

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "USER_ID", nullable = false)
    UserProfile user

    @OneToMany(mappedBy = "device", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JsonManagedReference
    @Builder.Default
    @JsonIgnore
    List<Subscription> subscriptions = new ArrayList<>()

    @Transient
    @JsonProperty(value = "subscriptions")
    Integer subCount = 0
}
