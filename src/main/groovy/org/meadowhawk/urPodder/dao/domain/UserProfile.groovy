package org.meadowhawk.urPodder.dao.domain

import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.JoinTable
import jakarta.persistence.ManyToMany
import jakarta.persistence.OneToMany
import jakarta.persistence.Transient
import lombok.AllArgsConstructor
import lombok.Builder
import lombok.Data
import lombok.NoArgsConstructor
import org.hibernate.annotations.DynamicUpdate
import org.springframework.security.core.userdetails.UserDetails

import java.time.LocalDateTime

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicUpdate
class UserProfile  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    String username
    String password
    String enabled = 'Y'

    @Transient
    @Builder.Default
    List<Authority> authorities = new ArrayList<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, targetEntity = Device.class)
    @JsonManagedReference
    @Builder.Default
    List<Device> devices = new ArrayList<>()

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JsonManagedReference
    @Builder.Default
    List<Subscription> subscriptions = new ArrayList<>()
}
