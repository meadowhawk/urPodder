package org.meadowhawk.urPodder.dao.domain

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.IdClass
import jakarta.persistence.Lob
import lombok.Builder
import org.meadowhawk.urPodder.dao.domain.enums.ReportType

import java.time.LocalDateTime
import java.time.ZoneOffset

@Entity
@IdClass(UserReportId.class)
class UserReport {

    @Id
    String username

    @Id
    ReportType reportType

    @Lob
    String report

    @JsonProperty("modified")
    @Builder.Default
    LocalDateTime modifiedAt = LocalDateTime.now(ZoneOffset.UTC)
}
