package org.meadowhawk.urPodder.dao.domain.enums

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

enum DeviceType {
    DESKTOP("Desktop"),
    LAPTOP("Laptop"),
    MOBILE("Cell phone"),
    SERVER("Server"),
    TABLET("Tablet"),
    OTHER("Other")

    String name
    DeviceType(String displayName){
        this.name = displayName
    }

    @JsonCreator
    static DeviceType forValues(String type) {
        for (DeviceType cond : values()) {
            if (cond.name() == type.toUpperCase()) {
                return cond
            }
        }

        return OTHER;
    }

    @JsonValue
    String getName() {
        return name
    }
}