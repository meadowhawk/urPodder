package org.meadowhawk.urPodder.dao.domain.enums

import com.fasterxml.jackson.annotation.JsonCreator

enum EpisodeActionType {
    DOWNLOAD,
    DELETE,
    PLAY,
    NEW

    @JsonCreator
    static EpisodeActionType forValues(String type) {
        for (EpisodeActionType cond : values()) {
            if (cond.name() == type.toUpperCase()) {
                return cond
            }
        }
        return null;
    }
}
