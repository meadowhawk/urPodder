package org.meadowhawk.urPodder.dao.domain.enums

enum ReportType {
    LAST_LISTEN,
    RECENT_EPISODES,
    RECENT_PODCASTS

}