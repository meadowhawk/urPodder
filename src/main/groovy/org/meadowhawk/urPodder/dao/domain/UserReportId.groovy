package org.meadowhawk.urPodder.dao.domain

import org.meadowhawk.urPodder.dao.domain.enums.ReportType

class UserReportId implements Serializable{
    String username
    ReportType reportType

    UserReportId(){}
    UserReportId(ReportType reportType, String username){}

    @Override
    boolean equals(Object o) {
        if (o == null || !(o instanceof UserReportId)) return false;
        UserReportId reportId = (UserReportId) o
        return username == reportId.username && reportType == reportId.reportType
    }

    @Override
    int hashCode() {
        return Objects.hash(username, reportType)
    }
}
