package org.meadowhawk.urPodder.dao.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.validation.constraints.NotNull
import lombok.Builder

import java.time.LocalDateTime
import java.time.ZoneOffset

@Entity
class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "DEVICE_ID", nullable = false)
    Device device

    URL refUrl

    @NotNull
    Long podcastId

    Boolean removed = false

    @JsonProperty("created")
    @Builder.Default
    LocalDateTime createdAt = LocalDateTime.now(ZoneOffset.UTC);

    @JsonProperty("modified")
    @Builder.Default
    LocalDateTime modifiedAt = LocalDateTime.now(ZoneOffset.UTC);

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "USER_ID", nullable = false)
    UserProfile user
}
