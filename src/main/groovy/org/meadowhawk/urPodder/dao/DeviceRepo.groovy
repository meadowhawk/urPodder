package org.meadowhawk.urPodder.dao

import org.meadowhawk.urPodder.dao.domain.Device
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DeviceRepo extends JpaRepository<Device, String> {
    Optional<Device> findByIdAndUserId(String deviceId, Long userId)
    List<Device> findByUserId(Long userId)
}
