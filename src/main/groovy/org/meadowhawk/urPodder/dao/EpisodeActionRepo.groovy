package org.meadowhawk.urPodder.dao

import jakarta.persistence.Tuple
import org.meadowhawk.urPodder.dao.domain.EpisodeAction
import org.meadowhawk.urPodder.dao.domain.enums.EpisodeActionType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface EpisodeActionRepo extends JpaRepository<EpisodeAction, Long> {
    List<EpisodeAction> findByActionAndUsernameOrderByClientTimestampDesc(EpisodeActionType action, String userName)

    @Query(value = recentEpisodesSQL  , nativeQuery = true)
    List<Tuple> findRecentEpisodesByUser(@Param("username") String username, @Param("rows") int count)

    def recentEpisodesSQL = """
select rec_eps.episode_url as episodeUrl,  p.url as podcastUrl FROM PODCAST p JOIN
(SELECT
ep.*
FROM  EPISODE_ACTION ep JOIN
(SELECT
    episode_url,
    MAX(CLIENT_TIMESTAMP) as updated
  FROM EPISODE_ACTION
  GROUP BY episode_url ORDER by updated desc) t2
ON ep.episode_url =  t2.episode_url
AND ep.CLIENT_TIMESTAMP = t2.updated
AND ACTION = 2
AND username  =  :username
ORDER by t2.updated desc
FETCH NEXT :rows ROWS ONLY
) rec_eps
ON rec_eps.PODCAST_ID = p.ID
"""
}