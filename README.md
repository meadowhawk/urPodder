# urPodder
![logo](static/upPodderLogoSm.png)

A Self-Hosting friendly Gpodder implementation and more, perfect for running on your home server.

## About
I just wanted something to manage my Podcasts and allow me to add custom features. I wanted it to be simple to set up and mostly self-contained. While there are nice options out there that support GPodder, they are in languages that I'm not super comfortable using.

So I build my own!


## Deployment
Initially there will be two options for deployment, local on a linux server or RaspberryPi and AutoDeploy to Railway. 
If you're not comfortable setting up njinx and ssl then the Railway option will be a quick win, only downside is it's likely to cost you 7-10$ a month.

### Host on RailwayApp (The Easy Button)
This is by far the quickest way to test this out to see if it will work for you! Once you set up an account on Railway simply hit the button to deploy. Based on my prior experience with Railway hosting, an app like this will run 5$ a month after the developer discount. (I haven't tested this so let me know if I'm off!) 

![Deploy on Railway](https://railway.app/button.svg)

Deploying this on Railway will create the app and deploy it in a couple minutes. It creates a separate volume for the database to ensure your data is saved and the app can be easily updated by redeploying, when new versions drop.

To verify, open the link to your instance from the link at the top of the Railway project. (It seems to take about 70 seconds before the public URL is active.) Once its public you'll be able to navigate to the welcome page.

Finally jump down to step 7 in local hosting to create an admin account.

### Local hosting (Linux/DietPi)
I am currently running my version of urPodder on a RaspberryPi-4 with the DietPi operating system. For a server I highly recommend using [DietPi](https://dietpi.com/), it's very actively maintained and thanks to their configuration application you can get a full server up and running with a few commands.

Specifically, I use DietPi to automatically set up services on my PI such as Nginx which I put to us as a proxy to my Java Apps as well as static pages. This will save a little configuration time since Nginx can be installed and configured with the Dietpi-software command.

Here is the steps for setting up a Java environment on your DietPi or the Pi of your choice:

1. ssh into your Pi, in my case I am using 
2. Install Nginx
3. Install sdkman and install Java 17 with it. (Ensure that both are installed for all users, root,pi,dietpi etc.)
4. Upload Jar file to the dietpi home directory for example: `/home/dietpi/apps`
5. Configure Nginx along with ssl keys
6. Setup urpodder.service with systemd nd start it.(Makes sure it restarts when the server reboots)
7. Create your admin user.
8. Sync!

#### 1. ssh to your Pi
   Something like this:  `ssh dietpi@192.168.x.x`, you will likely want to configure your Pi server with a static ip address in your router.

#### 2. Install Nginx
On diet this is as simple as running `dietpi-software` or you might have done it when you setup the PI initially with dietpi-configure?  

#### 3. Install SDKman and Java 17
So SDKMan makes managing Java very easy, if you don't know much about Java or haven't heard of sdkman, give it a try, it's the way!
- Install SDKMan - follow prompts
  ```bash
    curl -s "https://get.sdkman.io" | bash
  ```
- Install Java17 using sdkman (you can install any jdk you want that's v17, but I am running with Temurin)
  ```bash
      sdk install java 17.0.10-tem
  ```

#### 4. Upload upodder.jar
Download the executable jar file from the [repo releases](https://codeberg.org/meadowhawk/urPodder/releases) and Upload to your R-Pi. I use linux so I use rsync, this does require that ssl is enabled on your Pi!
```bash
  rsync --delete -pthrvz -e 'ssh -p 22' downloads/urPodder-[version-downloaded].jar  dietpi@192.xxx.xxx.xxx:~/apps/urPodder.jar
```

#### 5. Configure Nginx and ssl
Personally I have set this up using Nginx to provide my proxy to the internet. Also, rightfully, AntennaPod requires a secure connection for the app to connect to a server, so you need your site to support SSL. While ssl configurations can provide unwanted difficulties in a project, we've worked it out in a specific setup which is spelled out below. There are other ways to do this, this is the one it's been set up in my server.

My full setup involves   Cloudflare(SSL) -> Nginx -> java, With this application I found it necessary to add ssl certs to all levels. I'll be rebuilding this again soon and hope to report back that its not required for all 3, let me know if some of this is not required.

1. Setup Cloudflare with a subdomain A type and point to the server. Create a certificate following their [directions](https://developers.cloudflare.com/ssl/origin-configuration/origin-ca#deploy-an-origin-ca-certificate) and then download a copy of the cert and key, I used the pem format.
2. Configure Nginx via [my blog post](https://blog.meadowhawk.xyz/nginx-setup-for-java-with-ssl.html) and supplemental info with-in. I hate to repeat this here though I probably will later in a wiki entry.
3. Java - if you have saved the cert and key in the /etc/ssl/ location on your Pi then your good to go as that is the default location teh Springboot app will look for the certs!

#### 6.Setup Systemd
To make sure urPodder is running and restarts when ever the server restarts the easiest thing to do is set it up as a service using 'systemd'. 

##### Follow these steps to set up the service:
Below is a sample urpodder.service file. Not much needs changing here except possibly the path to the `java/current/bin/java` which might not be under /root.  Other things to note here, setting active.profile=prod is required.
One change you really should make is to  update the `DB_PASSWORD=password`, just change that password to something a bit harder to guess!
```properties
[Unit]
Description=UrPodder
Wants=network.target
After=syslog.target

[Service]
ExecStart=/root/.sdkman/candidates/java/current/bin/java -Dspring.profiles.active=prod -DDB_PASSWORD=password -DDB_PATH=/home/dietpi/apps/data/urPodder.db -jar /home/dietpi/apps/urPodder.jar
Restart=always
RestartSec=10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=urpodder
KillMode=mixed

[Install]
WantedBy=multi-user.target
```

#### 7. Admin User Setup
Once you have the server up and running you will want to create an Admin account. This account can do everything that a normal account can do, but it can also create new users!

The admin account gets created through a simple REST POST request and can be executed from the commandline with the following curl command: (All on one line)
  ```bash
  curl -X POST "https://<Server-DomainName>/admin/make-admin" -H "Content-Type: application/json" -d '{"username":"admin","password":"please-use-a-decent-password-and-save-it"}' 
  ```

There can be only one Admin account so be sure you note the username and password after you create it! If you have already created an admin user you will get an error message informing you of that. That's all it takes to set up your urPodder server!
 
## Building Source
This is a fairly straight forward build using Groovy,SpringBoot, H2 DB and Gradle for the build following the steps.
1. pull the latest from main.
2. Open in Intellij or build from the command line using `./gradelw clean build`
3. To run, you can run like any Java app using the commandline, see the upodder.service example for a rough idea of the command.
4. Running in an IDE, you can run the gradle Task 'bootRun', be sure to set the environment variable: `spring.profiles.active=dev```  

That should do it. Feel free to ask for help by opening an issue or on Mastodon. 

## Future
Some of the things I'm working on newt for this project:
- Add docs
- Add examples to Swagger Docs
- Add Open Podcast API support
- add Options for Auto-Posting to Social media or Federation?

